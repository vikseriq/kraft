<?php
/**
 * [K]raftman
 *
 * Self-hosted php server
 *
 * @copyright 2017 vikseriq <vikseriq@gmail.com>
 */

if (php_sapi_name() != 'cli-server'){
	return;
}

class KraftSelfServer {

	var $logger;

	function __construct(){
		$this->logger = fopen('php://stdout', 'w+');
	}

	function log($message){
		fprintf($this->logger, "[%s]\t%s\t%s: %s".PHP_EOL,
			date('Y-m-d H:i:sO'),
			$_SERVER['SERVER_NAME'],
			$_SERVER['REMOTE_ADDR'],
			$message);
	}

	function out_404(){
		header('Not Found', true, 404);
		return true;
	}

	function handle(){
		$this->log('Access: '.$_SERVER['SCRIPT_NAME']);

		$inner_path = realpath(__DIR__.$_SERVER['SCRIPT_NAME']);
		if (!$inner_path){
			return $this->out_404();
		}

		return false;
	}

}

$kss = new KraftSelfServer();

return $kss->handle();