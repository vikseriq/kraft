#!/usr/bin/env bash
# Starting local server with browser

phpVersion=$(php --version | tail -r | tail -n 1 | cut -d " " -f 2 | cut -c 1,3);
serverPort=8081;
openBrowser='open http://localhost:'$serverPort;

if (($phpVersion > 70)); then
	$openBrowser & php -S 0.0.0.0:$serverPort
else
	$openBrowser & python -m SimpleHTTPServer $serverPort
fi