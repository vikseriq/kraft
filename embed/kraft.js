(function(w){
  if (typeof w.KraftFormEmbed === 'undefined'){
    w.KraftFormEmbed = function(opt){
      if (!opt || !opt.window || !opt.formId || !opt.element || !opt.formProvider){
        if (window.console)
          console.warn('KraftFormEmbed: missing required options');
        return;
      }
      var el = opt.element;
      if (typeof el === 'string'){
        if (typeof window.document.querySelector !== 'undefined'){
          el = window.document.querySelector(el);
        } else {
          el = window.document.getElementById(el.substr(1));
        }
      }
      if (!el){
        if (window.console)
          console.warn('KraftFormEmbed: no `element` provided');
        return;
      }
      var frame = el.ownerDocument.createElement('iframe');
      frame.src = opt.formProvider + '/kraft.php?kraft_form=' + opt.formId;
      frame.width = '100%';
      frame.scrolling = 'no';
      frame.sandbox = 'allow-forms allow-scripts allow-same-origin allow-modals allow-popups';
      frame.frameBorder = 0;
      frame.height = 100;
      frame.id = 'kraft-embed-' + opt.formId;
      el.parentElement.replaceChild(frame, el);
      opt.window.addEventListener('message', function(e){
        var data = JSON.parse(e.data);
        if (!data || !data.formId)
          return;
        var el = window.document.getElementById('kraft-embed-' + data.formId);
        if (el){
          el.height = data.height;
        }
      });
    };

    w.KraftFormEmbedAsync = function(scriptElement){
      var doc = scriptElement.ownerDocument;
      var params = {};
      if (scriptElement.attributes['data-embed'])
        return;
      for(var x in scriptElement.attributes){
        var key = (scriptElement.attributes[x].name || '');
        if (!key || key.substr(0, 5) !== 'data-')
          continue;
        params[key] = scriptElement.attributes[x].value || '';
      }
      scriptElement.attributes['data-embed'] = true;
      var el = null;
      if (params['data-element']){
        el = doc.querySelector(params['data-element']);
      }
      if (!el){
        var parent = scriptElement.parentElement;
        el = document.createElement('div');
        el.id = 'kraft-embed-' + (+new Date());
        parent.replaceChild(el, scriptElement);
      }
      if (params['data-form-id']){
        w['kraft-embed-' + params['data-form-id']] = new KraftFormEmbed({
          window: w,
          formId: params['data-form-id'],
          element: el,
          formProvider: params['data-form-provider']
        });
      }
    }
  }

  if (typeof w.document.currentScript !== 'undefined'){
    KraftFormEmbedAsync(w.document.currentScript);
  }
})(window);