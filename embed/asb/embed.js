window.onResizeHandler = function(){
  var selfInner = window.document.getElementById('kraft-form-embed');
  if (selfInner){
    var height = Math.max(selfInner.offsetHeight, selfInner.clientHeight, selfInner.scrollHeight);
    window.parent.postMessage(JSON.stringify({
      formId: window.kraftForm.id,
      height: height + 40
    }), (window.location !== window.parent.location) ? document.referrer : document.location.href);
  }
};

window.addCaptchaCode = function(){
  var script = window.document.createElement('script');
  script.src = 'https://www.google.com/recaptcha/api.js';
  window.document.body.appendChild(script);
};

window.onresize = window.onResizeHandler;

var app = new Vue({
  el: '#kraft-form-app',

  data: {
    endpoint: '?kraft_api=antrag',
    endpoint_external: '?kraft_api=external',
    form: window.kraftForm,
    captchaCode: false,
    sent: false
  },

  render: function(h){
    return h('kraft-form-embed', {
      key: 'form-' + Math.random(),
      attrs: {
        id: 'kraft-form-embed'
      },
      props: {
        form: this.form,
        sent: this.sent,
        captchaCode: this.captchaCode
      }
    });
  },

  mounted: function(){
    this.$on('form.update', this.rebound);
    if (this.form.meta['recaptcha_site']){
      for (var x in this.form.fields){
        if (this.form.fields[x].type === 'system'
          && this.form.fields[x].code === 'captcha'){
          this.captchaCode = this.form.meta['recaptcha_site'];
          window.addCaptchaCode();
          break;
        }
      }
    }
  },

  methods: {
    httpSend: function(url, data, done, fail){
      if (typeof XMLHttpRequest === 'undefined')
        return fail('XMLHttpRequest not available');

      var xhr = new XMLHttpRequest();
      xhr.open('POST', url, true);
      xhr.setRequestHeader("Content-type", "application/json");
      xhr.onreadystatechange = function(){
        if (xhr.readyState === 4 && this.status < 400)
          done(xhr.responseText);
      };
      xhr.send(JSON.stringify(data));
    },

    send: function(values){
      var self = this;
      var data = values;
      data.form_id = this.form.id;

      this.httpSend(self.endpoint, data, function(){
        self.sent = true;
      }, function(){
        alert('Невозможно отправить форму');
      });
    },

    rebound: function(){
      this.$nextTick(function(){
        window.onResizeHandler();
      })
    },

    fetchExternal: function(key, callback){
      var self = this;
      var data_obj = {
        data: [], scheme: []
      };
      this.httpSend(self.endpoint_external, {
        external_directory: key
      }, function(raw){
        var data = data_obj;
        try {
          data = JSON.parse(raw);
        } catch (e) {
          data = data_obj;
        }
        callback(data);
      }, function(){
        callback(data_obj);
      });
    },

    baseType: function(type){
      switch (type){
        case 'checkbox':
        case 'radio':
          return 'check';
        case 'text':
        case 'textarea':
        case 'phone':
        case 'email':
        case 'select':
        case 'select_ext':
          return 'text';
        default:
          return type;
      }
    },

    passRule: function(rule, value){
      if (!value)
        return false;
      var v = (value + '').trim();
      switch (rule){
        case 'non_empty':
          return v.length > 0;
        case 'word_lat':
          return /^[a-zA-Z]{1,}/.test(v);
        case 'word_lat_num':
          return /^[0-9a-zA-Z]{1,}/.test(v);
        case 'word_cyr':
          return /^[\u0410-\u042F\u0430-\u044F\u0451\u045E\u0401\u040EiI\s]{1,}/.test(v);
        case 'word_cyr_num':
          return /^[\u0410-\u042F\u0430-\u044F\u0451\u045E\u0401\u040EiI0-9\s]{1,}/.test(v);
        case 'phone_by':
          return /^\+375\d{9}$/.test(v);
        case 'phone':
          return /^\+\d{10,15}$/.test(v);
        case 'email':
          return /(.+)@(.+){2,}\.(.+){2,}/.test(v);
        case 'number':
          return /^\d{1,}$/.test(v);
        case 'date':
          return /^\d{4}-\d{2}-\d{2}$/.test(v);
        default:
          try {
            var r = new RegExp(rule);
            return r.test(v);
          } catch (e) {
            console.warn('EmbedForm: malformed validation RegExp');
          }
          return true;
      }
    }
  }
});

// https://tc39.github.io/ecma262/#sec-array.prototype.find
if (!Array.prototype.find){
  Object.defineProperty(Array.prototype, 'find', {
    value: function(predicate){
      // 1. Let O be ? ToObject(this value).
      if (this == null){
        throw new TypeError('"this" is null or not defined');
      }

      var o = Object(this);

      // 2. Let len be ? ToLength(? Get(O, "length")).
      var len = o.length >>> 0;

      // 3. If IsCallable(predicate) is false, throw a TypeError exception.
      if (typeof predicate !== 'function'){
        throw new TypeError('predicate must be a function');
      }

      // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
      var thisArg = arguments[1];

      // 5. Let k be 0.
      var k = 0;

      // 6. Repeat, while k < len
      while (k < len){
        // a. Let Pk be ! ToString(k).
        // b. Let kValue be ? Get(O, Pk).
        // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
        // d. If testResult is true, return kValue.
        var kValue = o[k];
        if (predicate.call(thisArg, kValue, k, o)){
          return kValue;
        }
        // e. Increase k by 1.
        k++;
      }

      // 7. Return undefined.
      return undefined;
    }
  });
}