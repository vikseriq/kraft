
window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
								window.webkitRequestAnimationFrame || window.msRequestAnimationFrame || false;



var View = new (function()
{
	// private
	var body = $(document.body);
	var html = $(document.documentElement);
	var htmlbody = $([document.documentElement, document.body]);
	var win = $(window);
	var protectDropdownAnimTimeout = null;


	// public

	// проверка на мобильное устройство (true/false)
	this.mobileAndTabletCheck = (function()
	{
		var check = false;
		(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
		return check;
	})();


	this.scrollBarWidth;


	this.control = {

		// открыть выпадающее меню
		openDropdown: function(dropdown)
		{
			var requireQueue = $('.js-dropdown.open').length > 0;

			this.closeAllDropdowns(dropdown);

			if(requireQueue)
				dropdown.addClass('in-queue');

			dropdown.addClass('open animated');

			protectDropdownAnimTimeout = setTimeout(function()
			{
				dropdown.removeClass('animated');
			}, 300);
		},

		// закрыть выпадающее меню
		closeDropdown: function(dropdown)
		{
			clearTimeout(protectDropdownAnimTimeout);

			dropdown.removeClass('open animated');
		},

		// закрыть все выпадающие меню (кроме @except). exceptOptions = 'except-mouseover' - закрыть все, кроме того, который под курсором
		closeAllDropdowns: function()
		{
			this.closeDropdown(
				$('.js-dropdown').removeClass('related-open in-queue')
			);
			$('.js-dropdown__btn').removeClass('closed');
		},

		closeAll: function()
		{
			this.closeAllDropdowns();
		}
	};




	// инициализация
	this.init = {

		// global - инициализируется 1 раз
		global: {

			// инициализация позиционирования, не выходящего за экран или container
			tooltipPosition: function()
			{
				var tooltips;
				var delta;
				var i;
				var timeout = null;

				win.off('resize.setTooltipPositions').on('resize.setTooltipPositions', function()
				{
					if(!timeout)
						timeout = setTimeout(recalcPositions, 100);
					
				}).trigger('resize.setTooltipPositions');


				function recalcPositions() 
				{
					tooltips = $('.js-tooltip-position');
					tooltips.css('transition', 'none');
					tooltips.css('left', '');

					for(i = 0; i < tooltips.length; i++)
					{
						delta = tooltips[i].getBoundingClientRect().left + tooltips[i].offsetWidth - Math.min(window.innerWidth, window.innerWidth / 2 + 800) + View.scrollBarWidth + 10;

						if(delta > 0)
						{
							tooltips[i].style.left = ((parseInt(tooltips[i].style.left) || 0) - delta) + 'px';
						}
					}
					tooltips.css('transition', '');
					timeout = null;
				}
			},




			// инициализация вспомогательных скриптов для инпутов
			inputHelpers: function()
			{
				// вввод только цифр
				body.off('change.inputHelperNumber keyup.inputHelperNumber paste.inputHelperNumber', '.js-number-input')
					.on('change.inputHelperNumber keyup.inputHelperNumber paste.inputHelperNumber', '.js-number-input', function()
				{
					var t = $(this);
					var value = t.val();
					var oldValue = value;

					value = value.replace(/\D/g,'');

					if(oldValue !== value)
					{
						t.val(value);
					}
				});

				body.off('keydown.inputHelperNumber', '.js-number-input, .js-mask-input--units').on('keydown.inputHelperNumber', '.js-number-input, .js-mask-input--units', function(e)
				{
					var keyID = (window.event) ? event.keyCode : e.keyCode;

					if((keyID < 48 || keyID > 57) && (keyID < 96 || keyID > 105) && keyID !== 8 && keyID !== 9 && keyID !== 46 && keyID !== 116 && keyID !== 37 && keyID !== 39 && keyID !== 13) {
						return false;
					}
				});
			},



			// инпут со смещающимся плейсхолдером
			richTextInput: function()
			{
				body.off('change.markInputState paste.markInputState keyup.markInputState focus.markInputState blur.markInputState', '.js-rich-text-input__input')
					.on('change.markInputState paste.markInputState keyup.markInputState focus.markInputState blur.markInputState', '.js-rich-text-input__input', function()
				{
					var t = $(this);
					t.closest('.js-rich-text-input').toggleClass('active', t.is('select') || t.is('input, textarea') && this.value.length > 0);
				});

				body.off('click.focusRichTextInput', '.rich-text-input__label, .rich-text-input__icon')
					.on('click.focusRichTextInput', '.rich-text-input__label, .rich-text-input__icon', function()
				{
					$(this).closest('.js-rich-text-input').find('.js-rich-text-input__input').focus();
				});

				$('.js-rich-text-input__input').trigger('change.markInputState');
			},



			// инициализация селектов
			selects: function()
			{
				body.off('recalcSelectAutoWidth', '.js-select').on('recalcSelectAutoWidth', '.js-select', function()
				{
					var select = $(this);
					var selectVisual = select.closest('.js-select-visual');


					if(!select.is('.select--block, .select--embed, .select--lg, .select--md, .select--sm, .select--xs,  .select--open-type, .select--manual-width'))
					{
						selectVisual.css('display', 'inline-block');

						if(selectVisual.is(':visible'))
						{
							selectVisual
								.css('width', 'auto')
									.find('.select-list li')
										.css('white-space', 'nowrap');
							selectVisual
								.css('width', selectVisual.find('.select-list').width() + 20)
									.find('.select-list li')
										.css('white-space', '');
						}

						selectVisual.css('display', '');
					}
				});

				body.off('click.select', '.js-select-visual .select-list li')
					 .on('click.select', '.js-select-visual .select-list li', function()
				{
					if(this.disabled) return;

					var t = $(this);
					var visual = t.closest('.js-select-visual');
					var select = visual.find('select');

					if(select.hasClass('select--multiple'))
					{
						if(!t.hasClass('disabled'))
						{
							var option = select.find('option[value="'+t.attr('data-value')+'"]');
							option.prop('selected', !option.prop('selected'));
							select.trigger('change');
						}
					}
					else
					{
						select.val(t.attr('data-value')).trigger('change');
						visual.focus();
					}
				});

				body.off('click.openSelect', '.js-select-visual').on('click.openSelect', '.js-select-visual', function(e)
				{
					if(View.mobileAndTabletCheck) return;

					var t = $(this);

					if(t.find('select').is(':disabled')) return;

					if(!t.hasClass('select--multiple') || $(e.target).closest('.select-list').length === 0)
					{
						$('.js-select-visual.open').not(t).removeClass('open').find('li').attr('tabindex', '');
						t.toggleClass('open').find('li').attr('tabindex', t.hasClass('open') ? '0' : '');
					}
				});

				body.off('change.changeSelectVisual updateSelectVisual', 'select.js-select').on('change.changeSelectVisual updateSelectVisual', 'select.js-select', function(e)
				{
					var select = $(this);
					var visual = select.closest('.js-select-visual');
					var value = visual.find('.js-select-value');
					var selectedOptions = select.find('option:selected');
					var placeholder = select.attr('data-placeholder');
					visual.find('li').removeClass('active');

					if(e.type !== 'updateSelectVisual')
						select.add(visual).removeClass('placeholder-state');

					if(select.is('.js-select--link') && e.type !== 'updateSelectVisual')
					{
						window.open(selectedOptions[0].value);
						if(placeholder)
						{
							value.html(placeholder);
							return;
						}
					}

					selectedOptions.each(function()
					{
						visual.find('li[data-value="'+this.value+'"]').addClass('active');
					});

					if(select.hasClass('select--multiple'))
					{
						var selectedTxt = select.attr('data-selected-txt') || 'Выбрано';


						if(selectedOptions.length > 1)
							value.html(selectedTxt + ' ' + selectedOptions.length);
						else
							value.html(selectedOptions.html());

						if(visual.hasClass('has-placeholder') && selectedOptions.length === 0)
							visual.addClass('placeholder-state');
					}
					else
					{
						value.html(visual.find('li[data-value="'+select.val()+'"]').html());
					}
				});

				body.off('keyup.accessibilityClick', '.js-select-visual, .js-select-visual li')
					 .on('keyup.accessibilityClick', '.js-select-visual, .js-select-visual li', function(e)
				{
					e.stopPropagation();
					if(e.which==13 || e.which==32)
						$(this).trigger('click');
				});

				body.off('click.closeSelects').on('click.closeSelects', function(e)
				{
					var target = $(e.target);

					if(target.closest('.js-select-visual, select').length === 0)
					{
						$('.js-select-visual.open').removeClass('open');
					}
				});


				body.off('updateSelect', 'select.js-select')
					 .on('updateSelect', 'select.js-select', function()
				{
					$(this).trigger('updateSelectVisual').trigger('recalcSelectAutoWidth');
				});
			},




			switchSelect: function()
			{
				var updateTimeout = null;

				win.off('resize.updateSwitchSelects').on('resize.updateSwitchSelects', function()
				{
					if(!updateTimeout)
						updateTimeout = setTimeout(updateSwitchSelects, 200);
				});


				win.off('updateSwitchSelects').on('updateSwitchSelects', function()
				{
					updateSwitchSelects();
				});


				function updateSwitchSelects()
				{
					updateTimeout = null;

					$('.js-switch-select').each(function()
					{
						var t = $(this);
						var theSwitch = t.find('.js-switch-select__switch');
						// если элемент невидим, нужно его временно отобразить
						var farthestHiddenParent = t.parents(':hidden').last();

						farthestHiddenParent.show();
						t.removeClass('select-mode').toggleClass('select-mode', Math.ceil(theSwitch[0].scrollWidth) > Math.ceil(t.width()));
						farthestHiddenParent.css('display', '');
					});
				}


				body.off('change.switchSelectChangedBySelect', 'select.js-switch-select__select')
					 .on('change.switchSelectChangedBySelect', 'select.js-switch-select__select', function()
				{
					$(this).closest('.js-switch-select').find('.switch__input[value="'+this.value+'"]').prop('checked', true).trigger('change');
				});


				body.off('change.switchSelectChangedBySwitch', '.js-switch-select .switch__input')
					 .on('change.switchSelectChangedBySwitch', '.js-switch-select .switch__input', function()
				{
					$(this).closest('.js-switch-select').find('select.js-switch-select__select').val(this.value).trigger('updateSelect');
				});
			},



			accordions: function()
			{
				body.off('click.toggleAccordion keyup.toggleAccordion', '.js-accordion__btn, .text-guide dt').on('click.toggleAccordion keyup.toggleAccordion', '.js-accordion__btn, .text-guide dt', function(e)
				{
					if(e.type == 'keyup' && e.keyCode !== 13)
						return;

					var btn = $(this);
					var accordion = btn.closest('.js-accordion, dl');
					var accordionBody = accordion.is('dl') ? btn.next() : $(accordion.find('.js-accordion__body').not(accordion.find('.js-accordion .js-accordion__body')));
					accordion.toggleClass('open');

					if(accordion.hasClass('open'))
						accordionBody.stop().hide().slideDown(350);
					else
						accordionBody.stop().show().slideUp(350);
				});
			},



			/*
			loadSvgSprite: function()
			{
				$.ajax({
					url: '/public/images/sprite.svg',
					method: 'GET',
					dataType: 'text'
				}).done(function(response)
				{
					body.prepend(response);
				});
			},
			*/
		},


		// local - требует переинициализации после перезагрузки DOM
		local: {

			slidableInput: function(scope)
			{
				// не инициализируем лишние обработчики, если таких инпутов на странице нет
				if($('.js-slidable-input').length == 0)
					return;

				var dragMode = false;
				var currentHandle;
				var currentInput;
				var currentInputOffset;
				var currentInputWidth;
				var currentInputMinVal;
				var currentInputMaxVal;
				var currentInputStep;
				var relativeValue;
				var pointerOffset;

				$('.js-slidable-input__input', scope).off('change.slidableInputChange keyup.slidableInputChange paste.slidableInputChange')
													 .on('change.slidableInputChange keyup.slidableInputChange paste.slidableInputChange', function()
				{
					if(dragMode)
						return;
					
					setInput($(this), $(this).closest('.js-slidable-input').find('.js-slidable-input__handle'))
					inputChange();
				});

				$('.js-slidable-input__handle', scope).off('mousedown.slidableInputDragStart touchstart.slidableInputDragStart')
													   .on('mousedown.slidableInputDragStart touchstart.slidableInputDragStart', function()
				{
					dragMode = true;
					setInput($(this).closest('.js-slidable-input').find('.js-slidable-input__input'), $(this));
				});

				body.off('mouseup.slidableInputDragEnd dragend.slidableInputDragEnd touchend.slidableInputDragEnd')
					 .on('mouseup.slidableInputDragEnd dragend.slidableInputDragEnd touchend.slidableInputDragEnd', function()
				{
					dragMode = false;
				});

				body.off('mousemove.slidableInputDrag touchmove.slidableInputDrag').on('mousemove.slidableInputDrag touchmove.slidableInputDrag', function(e)
				{
					if(!dragMode)
						return;

					pointerOffset = e.type == 'mousemove' ? e.pageX : e.touches[0].pageX;

					e.preventDefault();

					if(requestAnimationFrame)
						requestAnimationFrame(sliderChange)
					else
						sliderChange();

					return false;
				});

				function sliderChange()
				{
					sliderToInput();
					setPosition();
				}

				function inputChange()
				{
					inputToSlider();
					setPosition();
				}

				function setInput(input, handle)
				{
					currentHandle = handle;
					currentInput = input;
					currentInputOffset = currentInput.offset().left;
					currentInputWidth = currentInput.outerWidth();
					currentInputMinVal = currentInput.attr('data-min-value');
					currentInputMaxVal = currentInput.attr('data-max-value');
					currentInputStep = currentInput.attr('data-step') || 0.0001;
				}

				function setPosition()
				{
					currentHandle.css('left', relativeValue * 100 + '%');
					currentInput.css('background-size', relativeValue * 100 + '% 100%');
				}

				function sliderToInput()
				{
					relativeValue = Math.min(1, Math.max(0, (pointerOffset - currentInputOffset) / currentInputWidth));

					currentInput.val(Math.round(currentInputMinVal + relativeValue * (currentInputMaxVal - currentInputMinVal) / currentInputStep) * currentInputStep);
					currentInput.trigger('formatMaskInputUnits').trigger('change');
				}

				function inputToSlider()
				{
					var cleanValue = currentInput.val();
					var integer = currentInput.attr('data-mask-integer') === 'true';

					cleanValue.toString()
						.replace(/[^0-9$.,]/g, '')
						.replace(/,/g, '.')
						.replace(/\s/g, "");

					if(integer)
						cleanValue = parseInt(cleanValue) || 0;
					else
						cleanValue = parseFloat(cleanValue) || 0;

					relativeValue = Math.min(1, Math.max(0, (cleanValue - currentInputMinVal) / (currentInputMaxVal - currentInputMinVal)));
				}
			},



			// инициализация маски ввода для телефонов
			maskInput: function(scope)
			{
				$('.js-mask-input', scope).not('.is-stl').each(function()
				{
					$(this).inputmask({showMaskOnHover: false}).addClass('is-stl');
				});


				$(".js-mask-input--phone", scope).inputmask({mask: "+375 99 999-99-99", placeholder: '+375 __ ___-__-__', clearMaskOnLostFocus: true, showMaskOnHover: false});
				$(".js-mask-input--date", scope).inputmask({mask: "99.99.9999", placeholder: '__.__.____', clearMaskOnLostFocus: true, showMaskOnHover: false});
				$(".js-mask-input--passport-num", scope).inputmask({mask: "AA 9999999", placeholder: '__ _______', clearMaskOnLostFocus: true, showMaskOnHover: false});
				$(".js-mask-input--passport-id", scope).inputmask({mask: "9999999A999AA9", placeholder: '______________', clearMaskOnLostFocus: true, showMaskOnHover: false});

				$('.js-mask-input--units', scope).off('focus.clearMaskInputUnits').on('focus.clearMaskInputUnits', function()
				{
					if(!this.value)
						return;
					var t = this;

					setTimeout((function() {
						var caretPos = 0;
						var newCaretPos = 0;
						// IE Support
						if (document.selection)
						{
							// To get cursor position, get empty selection range
							var sel = document.selection.createRange();
							// Move selection start to 0 position
							sel.moveStart('character', -this.value.length);
							// The caret position is selection length
							caretPos = sel.text.length;
						}
						// Firefox support
						else if (this.selectionStart || this.selectionStart == '0')
							caretPos = this.selectionStart;

						// добавляю количество пробелов до курсора, чтобы правильно установить курсор
						newCaretPos = Math.max(0, caretPos - this.value.slice(0,caretPos).split(' ').length + 1);

						var result = this.value;
						var integer = $(this).attr('data-mask-integer') === 'true';

						result = result.toString()
								.replace(/[^0-9$.,]/g, '')
								.replace(/,/g, '.')
								.replace(/\s/g, "");

						if(integer)
							this.value = parseInt(result) || 0;
						else
							this.value = (parseFloat(result) || 0).toFixed(2).replace(/\./g, ',');

						this.setSelectionRange(newCaretPos,newCaretPos);
					}).bind(this), 100);

				});

				$('.js-mask-input--units', scope).off('blur.restoreMaskInputUnits formatMaskInputUnits').on('blur.restoreMaskInputUnits formatMaskInputUnits', function()
				{
					if(!this.value)
						return;

					var result = this.value;
					var units = $(this).attr('data-mask-units');
					var integer = $(this).attr('data-mask-integer') === 'true';

					result = result.toString()
								.replace(/[^0-9$.,]/g, '')
								.replace(/,/g, '.')
								.replace(/\s/g, "");

					if(integer)
						this.value = (parseInt(result) || 0).toString().replace(/(\d)(?=(\d{3})+$)/g, '$1 ') + (units ? ' ' + units : '');
					else
						this.value = (parseFloat(result) || 0).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1 ').replace(/\./g, ',') + (units ? ' ' + units : '');
				});
			},



			// инпут со смещающимся плейсхолдером
			richTextInuptLocal: function(scope)
			{
				$('.js-rich-text-input_input', scope).trigger('change.markInputState');
			},



			// инициализация селектов
			selectsLocal: function(scope)
			{
				var selects = $(scope);
				if(!selects.is('select')) selects = $('select.js-select', scope);

				selects.each(function()
				{
					var select = $(this);
					var selectVisual = select.closest('.js-select-visual');
					var selectList;
					var selectChevron;
					var selectedText;
					var multiple = this.multiple;
					var disabled = this.disabled;
					var placeholder = select.attr('data-placeholder');

					selectedText = select.find('option:selected');

					if(selectedText.length > 0) selectedText = selectedText.html();
					else selectedText = select.find('option:first-child').html();

					if(selectVisual.length)
					{
						selectVisual.children().not(select).remove();
						select.unwrap('.js-select-visual');
					}

					selectVisual = $('<div class="js-select-visual '+select.attr('class')+'" tabindex="0"></div>');
					selectValue = $('<span class="select-value js-select-value">'+selectedText+'</span>');
					selectChevron = $('<span class="select-chevron"></span>');
					selectList = $('<ul class="select-list"></ul>');

					var option;
					var optionContent;
					var optionClass;
					select.find('option').each(function()
					{
						option = $(this);
						optionContent = option.html();
						optionClass = 'class="';
						optionValue = option.attr('value');

						if(!optionValue)
							return;

						if(multiple)
							optionContent = '<span class="checkbox-row__visual"></span>' + optionContent;

						if(option.attr('value') === select.val())
							optionClass += 'active ';
						if(option.is(':disabled'))
							optionClass += 'disabled ';

						optionClass += '"';

						selectList.append(
							'<li data-value="'+optionValue+'" '+optionClass+'>' + optionContent + '</li>');
					});

					var selectAndVisual = select.add(selectVisual);
					selectAndVisual.toggleClass('select--multiple', multiple);
					selectAndVisual.toggleClass('disabled', disabled);
					selectAndVisual.toggleClass('has-placeholder', !!placeholder);
					selectAndVisual.toggleClass('placeholder-state', !!placeholder);

					if(!!placeholder)
					{
						selectVisual.prepend('<span class="select-placeholder">'+placeholder+'</span>')
					}

					selectVisual.append(selectValue).append(selectChevron).append(selectList).removeClass('js-select js-alt-tab-controller js-rich-text-input__input');
					select.after(selectVisual);
					selectVisual.prepend(select);

				}).trigger('updateSelect').trigger('recalcSelectAutoWidth');
			},



			switchSelectsLocal: function(scope)
			{
				$('.js-switch-select', scope).each(function()
				{
					var t = $(this);
					var inputs = t.find('.switch__input');
					var selectHTML = '<select class="select select--block js-select js-switch-select__select" '+(inputs.filter('[type="checkbox"]').length ? 'multiple' : '')+'>';
					inputs.each(function()
					{
						selectHTML += '<option value="' + this.value + '">' + this.parentNode.innerText + "</option>";
					});
					selectHTML += '</select>';

					t.find('.js-switch-select__select').remove();
					t.append(selectHTML);
				});

				win.trigger('updateSwitchSelects');

				$('.js-switch-select', scope).addClass('initialized');

				View.init.local.selectsLocal(scope);
			},



			dropdowns: function(scope) {
				var showTimeout = null, hideTimeout = null;

				// проверка на соответствие разрешению экрана
				function checkInitBreakpoint(dropdown)
				{
					var initModeMax = dropdown.attr('data-dropdown-init-max');
					var initModeMin = dropdown.attr('data-dropdown-init-min');
					var winWidth = window.innerWidth;

					if(initModeMax)
						if(initModeMax === 'sm' && winWidth >= 1080 ||
						   initModeMax === 'xs' && winWidth >= 768) return false;

					if(initModeMin)
						if(initModeMin === 'lg' && winWidth < 1080 ||
						   initModeMin === 'sm' && winWidth < 768) return false;

					return true;
				}

				if(this.mobileAndTabletCheck)	// на телефонах / планшетах открытие по клику
				{
					$('.js-dropdown__btn', scope).off('click.openDropdown').on('click.openDropdown', function(e)
					{
						var btn = $(this);
						var dropdown = btn.closest('.js-dropdown');

						// если выпадалка не должна работать для этого разрешения - выход
						if(!checkInitBreakpoint(dropdown)) return;

						if(btn.hasClass('open'))
						{
							View.control.closeDropdown(dropdown);
						}
						else
						{
							View.control.openDropdown(dropdown);
							e.preventDefault();
						}
					});
				}
				else 	// на десктопе открытие по наведению
				{
					// при остановке мыши на выпадающем меню - открытие меню
					$('.js-dropdown__btn', scope).off('mousemove.openDropdown').on('mousemove.openDropdown', function()
					{
						var btn = $(this);

						if(btn.hasClass('closed')) return;

						var dropdown = btn.closest('.js-dropdown');

						// если выпадалка не должна работать для этого разрешения - выход
						if(!checkInitBreakpoint(dropdown)) return;

						clearTimeout(showTimeout);
						clearTimeout(hideTimeout);
						showTimeout = hideTimeout = null;

						// ждем остановки курсора
						if(!dropdown.hasClass('open'))
						{
							showTimeout = setTimeout(function()
							{
								clearTimeout(hideTimeout);
								hideTimeout = showTimeout = null;

								View.control.openDropdown(dropdown);
							}, 70);
						}
					});

					// при наведении мыши на выпадающее меню - отмена скрытия меню
					$('.js-dropdown__btn, .js-dropdown__body', scope).off('mouseenter.openDropdown').on('mouseenter.openDropdown', function()
					{
						var dropdown = $(this).closest('.js-dropdown');

						// если выпадалка не должна работать для этого разрешения - выход
						if(!checkInitBreakpoint(dropdown)) return;

						if(dropdown.hasClass('open'))
						{
							clearTimeout(hideTimeout);
							hideTimeout = null;
						}
					});

					// при уведении мыши с выпадающего меню - запуск таймера на скрытие меню
					$('.js-dropdown__btn, .js-dropdown__body', scope).off('mouseleave.closeDropdown').on('mouseleave.closeDropdown', function()
					{
						// если выпадалка не должна работать для этого разрешения - выход
						if(!checkInitBreakpoint($(this).closest('.js-dropdown'))) return;

						clearTimeout(showTimeout);
						showTimeout = null;

						hideTimeout = setTimeout(function()
						{
							View.control.closeAllDropdowns();
							hideTimeout = null;
						}, 400);
					});

					// при клике на открытый пункт меню - закрытие
					$('.js-dropdown__btn', scope).off('click.closeDropdown').on('click.closeDropdown', function()
					{
						var btn = $(this);
						var dropdown = btn.closest('.js-dropdown');

						// если выпадалка не должна работать для этого разрешения - выход
						// если выпадалка в данный момент открывается - тоже выход
						if(!checkInitBreakpoint(dropdown) || showTimeout || dropdown.hasClass('animated')) return;


						if(dropdown.hasClass('open'))
						{
							View.control.closeAllDropdowns();

							btn.addClass('closed');
						}
						else if(!dropdown.hasClass('js-notification'))
						{
							View.control.openDropdown(dropdown);
						}
					});
				}


				// кнопка закрытия выпадающих меню
				body.off('click.closeDropdown', '.js-dropdown__close').on('click.closeDropdown', '.js-dropdown__close', function()
				{
					View.control.closeAllDropdowns();
				});
			},



			tooltipPositionLocal: function()
			{
				win.trigger('resize.setTooltipPositions');
			},



			stopPrevent: function(scope)
			{
				$('.js-stop-prevent', scope).off('click.stopPrevent').on('click.stopPrevent', function(e)
				{
					e.preventDefault();
					e.stopPropagation();
				});
			},
		}
	};



	this.initView = function()
	{
		body = $(document.body);
		htmlbody = $([document.documentElement, document.body]);
		win = $(window);

		this.scrollBarWidth = (function()
		{
			var outer = document.createElement("div");
			outer.style.visibility = "hidden";
			outer.style.width = "100px";
			outer.style.msOverflowStyle = "scrollbar";

			document.body.appendChild(outer);

			var widthNoScroll = outer.offsetWidth;
			// force scrollbars
			outer.style.overflow = "scroll";

			// add innerdiv
			var inner = document.createElement("div");
			inner.style.width = "100%";
			outer.appendChild(inner);

			var widthWithScroll = inner.offsetWidth;

			// remove divs
			outer.parentNode.removeChild(outer);

			return widthNoScroll - widthWithScroll;
		})();
	}



	this.initAllGlobal = function()
	{
		$.each(this.init.global, function(index, fn)
		{
			if(typeof fn === 'function') fn();
		});
	};



	this.initAllLocal = function(scope)
	{
		$.each(this.init.local, function(index, fn)
		{
			if(typeof fn === 'function') fn(scope || document.body);
		});
	};
})();


$(document).ready(function()
{
	var body = $(document.body);

	body.addClass(View.mobileAndTabletCheck ? 'touch' : 'no-touch');

	View.initView();
	View.initAllGlobal();
	View.initAllLocal(body);

});


