<?php
/**
 * [K]raftman
 *
 * Kraft: Blazing fast landings web service
 *
 * @copyright 20!8 vikseriq <vikseriq@gmail.com>
 */

/**
 * Primary configuration for instance
 */
class KraftConfig {
	const kraft_path = __DIR__.'/kraftman/';
	const kraft_forms_path = self::kraft_path.'forms/';
	const database_config = 'sqlite3;'.self::kraft_forms_path.'/forms.db;';
	const forms_embed_path = __DIR__.'/embed/asb/';
}

include KraftConfig::kraft_path.'kraft.api.php';
include KraftConfig::kraft_path.'kraft.builder.php';
include KraftConfig::kraft_path.'kraft.form.php';

if (empty($_POST) && !empty($_GET['kraft_form'])){
	// Form embed
	include KraftConfig::kraft_path.'kraft.form.embed.php';
	$kfe = new KraftFormEmbed();
	$kfe->process($_GET);
} elseif (defined('KRAFT_ENTRY') && empty($_GET['kraft_api']) && empty($_POST['kraft_api'])){
	// Landing server
	$kb = new KraftBuilder(KRAFT_ENTRY, __DIR__);
	$kb->process();
} else {
	// API
	$api = new KraftApi(__DIR__);
	$api->handle();
}