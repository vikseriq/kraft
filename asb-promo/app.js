define(function(){return {
    "creature": {
        "id": "asb-page-one",
        "name": "BelarusBank rev.0.1",
        "path": "\/lp-one\/",
        "server_path": "\/asb-promo\/",
        "ziegel": "asb",
        "htmlMeta": "<title>Это заголовок<\/title>",
        "htmlBody": "<script>console.log('Пример встраивания скрипта')<\/script>",
        "updated": "2018-01-12T05:29:02+00:00",
        "htmlTitle": "Навстречу приключениям"
    },
    "slides": [
        {
            "id": "one",
            "type": "asb-head-text",
            "title": "Вводный",
            "props": {
                "headTitle": "💸 Кредит наличными сразу в руки 🙈",
                "headText": "<strong>Без дополнительных пакетов услуг и страховок. Получите ваши деньги сразу в руки. Привилегии зарплатным клиентам.<\/strong>",
                "background": "linear-gradient(to right, rgba(1,115,74,.5), rgba(0,0,0,0)), url(..\/asb\/images\/markup-images\/backgrounds\/background-2-no-light.jpg);",
                "contactPhone": "Контактный <br> телефон",
                "header_transparent": "",
                "phone": "147",
                "title_form": "Получите консультацию",
                "subtitle_form": "Бесплатно. Ни к чему не обязывает.",
                "button_form": "Получите консультацию",
                "form_note": "Консультант перезвонит через 15 минут",
                "form": "gaguuhi5ut",
                "menu": [
                    {
                        "link": "#time",
                        "text": "Кредит наличными"
                    },
                    {
                        "link": "#phone",
                        "text": "Консультация"
                    },
                    {
                        "link": "http:\/\/asb.by\/",
                        "text": "Сайт"
                    }
                ]
            }
        },
        {
            "id": "cgvdplzf4t",
            "type": "asb-green-line",
            "title": "Этаж",
            "props": {
                "link": "https:\/\/belarusbank.by\/",
                "phone": "147",
                "comment_phone": "Контактный <br> телефон"
            },
            "code": "phone"
        },
        {
            "id": "0hroe2fj5o",
            "type": "asb-infographics",
            "title": "Инфографика",
            "code": "",
            "props": {
                "title": "Заголовок",
                "subtitle": "Подзаголовок",
                "background": "url(..\/asb\/images\/markup-images\/backgrounds\/background-4.jpg)",
                "items": [
                    {
                        "title": "Особые условия",
                        "text": "Для зарплатных клиентов",
                        "link": "#",
                        "icon": "",
                        "image": "url(..\/asb\/images\/markup-images\/visuals\/visual-1.png);"
                    },
                    {
                        "title": "Ставка 6%",
                        "text": "На уровне топовых банков",
                        "link": "",
                        "icon": "..\/asb\/images\/markup-images\/icons\/icon-1.png",
                        "image": "url(..\/asb\/images\/markup-images\/visuals\/visual-1.png);"
                    }
                ],
                "code": "",
                "btn_outline": true
            }
        },
        {
            "id": "f954q4zlhl",
            "type": "asb-countdown",
            "title": "Слайд",
            "preview": null,
            "props": {
                "title": "Успейте получить Visa Gold до завершения акции",
                "subtitle": "Более 100500 заявок за N месяца",
                "background": "url(\/asb\/images\/markup-images\/backgrounds\/background-6.jpg)",
                "timer": "2018-12-31",
                "code": "time"
            },
            "code": "time"
        },
        {
            "id": "9blwmv243w",
            "type": "asb-form-horizontal-popup",
            "title": "Этаж",
            "props": {
                "title": "Получите консультацию",
                "subtitle": "Бесплатно. Ни к чему не обязывает.",
                "button_form": "Получите консультацию",
                "button_popup": "Открыть окно",
                "form_title": "Получите консультацию",
                "form_note": "Консультант перезвонит через 15 минут",
                "form": "hj5scv0oej"
            }
        },
        {
            "id": "7mi7isfocl",
            "type": "asb-info-video",
            "title": "Этаж",
            "code": "",
            "props": {
                "code": "better",
                "info_title": "Заголовок",
                "info_text": "<h3>Что еще нужно знать?<\/h3><p>Текст<\/p>",
                "info_background": "url(..\/asb\/images\/markup-images\/backgrounds\/background-8.jpg)",
                "video_title": "Лучше один раз увидеть!",
                "video_src": "http:\/\/techslides.com\/demos\/sample-videos\/small.mp4",
                "video_background": "linear-gradient(to bottom, #393637, #231f20)",
                "items": [
                    {
                        "title": " хороший процент",
                        "icon": "..\/asb\/images\/markup-images\/icons\/icon-1.png"
                    }
                ]
            }
        },
        {
            "id": "60q8yogzfn",
            "type": "asb-footer",
            "title": "Этаж",
            "code": "",
            "props": {
                "footnote": "Лицензия ЦБ РБ №1 от 24.05.2013",
                "show_social": true
            }
        }
    ]
}
});