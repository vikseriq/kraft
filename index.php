<?php
// Expose landings list
$path = __DIR__;
$files = scandir($path);

$paths = [];
foreach ($files as $file){
	if ($file[0] == '.')
		continue;
	if (file_exists($path.'/'.$file.'/app.js'))
		$paths[$file.'/'] = $file;
	elseif ($file == 'kraftman')
		$paths[$file.'/'] = $file;
	elseif (substr($file, strrpos($file, '.')) === '.html')
		$paths[$file] = $file;
}
?>
<h1>Kraftwerk</h1>
<?php if (empty($paths)): ?>
	<p>Environment is empty</p>
<?php else: ?>
	<ul>
		<?php foreach ($paths as $key => $path):
			printf('<li><a href="%s">%s</a></li>', $key, $path);
		endforeach; ?>
	</ul>
<?php endif ?>
