define(function(){
  return {
    "id": "asb",
    "name": "Лендинги «Беларусбанк» 2018",
    "files": {
      "path": "asb/",
      "base": "index.html",
      "parts": [
        "ziegel/kraft-html.vue",
        "ziegel/kraft-form.vue",
        "ziegel/asb-head-text.vue",
        "ziegel/asb-head-form.vue",
        "ziegel/asb-footer.vue",
        "ziegel/asb-green-line.vue",
        "ziegel/asb-form-horizontal.vue",
        "ziegel/asb-form-steps.vue",
        "ziegel/asb-video-text.vue",
        "ziegel/asb-infographics.vue",
        "ziegel/asb-info-video.vue",
        "ziegel/asb-info-countdown.vue",
        "ziegel/asb-banner-background.vue",
        "ziegel/asb-form-horizontal-popup.vue",
        "ziegel/asb-countdown.vue"
      ]
    }
  }
});