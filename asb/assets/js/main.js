
window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
								window.webkitRequestAnimationFrame || window.msRequestAnimationFrame || false;


var View = new (function()
{
	// private
	var body = $(document.body);
	var htmlbody = $([document.documentElement, document.body]);
	var win = $(window);
	var protectDropdownAnimTimeout = null;


	// public

	// проверка на мобильное устройство (true/false)
	this.mobileAndTabletCheck = (function()
	{
		var check = false;
		(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
		return check;
	})();


	this.isIE = (function()
	{
		var ua = window.navigator.userAgent;

		var msie = ua.indexOf('MSIE ');
		if(msie > 0) return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);

		var trident = ua.indexOf('Trident/');
		if(trident > 0)
		{
			var rv = ua.indexOf('rv:');
			return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
		}

		var edge = ua.indexOf('Edge/');
		if(edge > 0) return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);

		return false;
	})();


	this.control = {
		openTopMessage: function(type, text)
		{
			this.closeTopMessages();

			if(type === 'success')
				$('.js-top-message__success').addClass('open').find('.js-top-message_text').html(text);

			else if(type === 'error')
				$('.js-top-message__error').addClass('open').find('.js-top-message_text').html(text);

			fixedMessageTimeout = setTimeout(function()
			{
				View.control.closeTopMessages();
			}, 3500);
		},

		closeTopMessages: function()
		{
			$('.js-top-message.open').removeClass('open');
		},

		openModalByURL: function(url)
		{
			$('.js-modal-container').remove();
			body.append('<div class="modal-container js-modal-container"></div>');
			body.addClass('state--modal-loading');

			$.get(url, function(data)
			{
				$('.js-modal-container').append(data);
				View.control.openModal($('.js-modal'));
			});
		},

		openModal: function(modal, noInitRequired)
		{
			View.control.closeAll();

			modal.addClass('loading').addClass('visible');

			var modalHeight = modal.height();
			var winHeight = win.height();
			var winScrollTop = win.scrollTop();
			var offsetParent = modal.offsetParent();

			if(!offsetParent.is('body'))
				winScrollTop -= offsetParent.offset().top;

			var modalTop = modalHeight > winHeight ? winScrollTop + 50 : winScrollTop + (winHeight - modalHeight) / 2;

			if(modalTop + modalHeight + 300 > body.height())
			{
				modalTop = body.height() - modalHeight - 300;
				htmlbody.animate({scrollTop: modalTop - 50}, Math.abs(modalTop - winScrollTop));
			}

			modal.css('top', modalTop);

			if(!noInitRequired)
				View.initAllLocal(modal);

			setTimeout(function()
			{
				body.removeClass('state--modal-loading').addClass('state--modal-open');
				modal.removeClass('loading').addClass('open');
			}, 0);
		},

		closeModal: function()
		{
			body.removeClass('state--modal-loading state--modal-open');
			$('.js-modal').removeClass('open').removeClass('visible');
		},

		closeAll: function()
		{
			this.closeTopMessages();
			this.closeModal();
		},

    scrollToElement: function(target, options){
      var additionalOffset = 0;
      options = $.extend({
      	center: false,
				animate: true
			}, options || {});

      if(target.length === 0)
      {
        console.warn('js-anchor: wrong data-target or href', t);
        return;
      }

      if(options.center && target.outerHeight() < window.innerHeight - 120)
      {
        additionalOffset = (window.innerHeight - target.outerHeight()) / 2;
      }


      // отмена стандартного поведения ссылок с якорем, если анимированный скролл не потребуется
      if(target.offset().top - additionalOffset === win.scrollTop()) e.preventDefault();

      var y = target.offset().top - additionalOffset;

      if (options.animate){
        htmlbody.animate({scrollTop: y}, Math.min(900, Math.abs(win.scrollTop() - (target.offset().top - additionalOffset)) * 0.8));
      } else {
      	htmlbody.scrollTop(y);
			}
    }
	};




	// инициализация
	this.init = {

		// global - инициализируется 1 раз
		global: {

			// инпут со смещающимся плейсхолдером
			richTextInput: function()
			{
				body.off('change.markInputState paste.markInputState keyup.markInputState focus.markInputState blur.markInputState', '.js-rich-text-input__input')
					.on('change.markInputState paste.markInputState keyup.markInputState focus.markInputState blur.markInputState', '.js-rich-text-input__input', function()
				{
					var t = $(this);
					t.closest('.js-rich-text-input').toggleClass('active', t.is('select') || t.is('input, textarea') && this.value.length > 0);
				});

				body.off('click.focusRichTextInput', '.rich-text-input__label, .rich-text-input__icon')
					.on('click.focusRichTextInput', '.rich-text-input__label, .rich-text-input__icon', function()
				{
					$(this).closest('.js-rich-text-input').find('.js-rich-text-input__input').focus();
				});

				$('.js-rich-text-input__input').trigger('change.markInputState');
			},



			// инициализация селектов
			selects: function()
			{
				body.off('recalcSelectAutoWidth', '.js-select').on('recalcSelectAutoWidth', '.js-select', function()
				{
					var select = $(this);
					var selectVisual = select.closest('.js-select-visual');


					if(!select.is('.select--block, .select--embed, .select--lg, .select--md, .select--sm, .select--xs,  .select--open-type, .select--manual-width'))
					{
						selectVisual.css('display', 'inline-block');

						if(selectVisual.is(':visible'))
						{
							selectVisual
								.css('width', 'auto')
									.find('.select-list li')
										.css('white-space', 'nowrap');
							selectVisual
								.css('width', selectVisual.find('.select-list').width() + 20)
									.find('.select-list li')
										.css('white-space', '');
						}

						selectVisual.css('display', '');
					}
				});

				body.off('click.select', '.js-select-visual .select-list li')
					 .on('click.select', '.js-select-visual .select-list li', function()
				{
					var t = $(this);
					var visual = t.closest('.js-select-visual');
					var select = visual.find('select');

					if(select.hasClass('select--multiple'))
					{
						if(!t.hasClass('disabled'))
						{
							var option = select.find('option[value="'+t.attr('data-value')+'"]');
							option.prop('selected', !option.prop('selected'));
							select.trigger('change');
						}
					}
					else
					{
						select.val(t.attr('data-value')).trigger('change');
						visual.focus();
					}
				});

				body.off('click.openSelect', '.js-select-visual').on('click.openSelect', '.js-select-visual', function(e)
				{
					if(View.mobileAndTabletCheck) return;

					var t = $(this);

					if(!t.hasClass('select--multiple') || $(e.target).closest('.select-list').length === 0)
					{
						$('.js-select-visual.open').not(t).removeClass('open').find('li').attr('tabindex', '');
						t.toggleClass('open').find('li').attr('tabindex', t.hasClass('open') ? '0' : '');
					}
				});

				body.off('change.changeSelectVisual', '.js-select').on('change.changeSelectVisual', '.js-select', function(e)
				{
					var select = $(this);
					var visual = select.closest('.js-select-visual');
					var value = visual.find('.js-select-value');
					var selectedOptions = select.find('option:selected');
					visual.find('li').removeClass('active');

					selectedOptions.each(function()
					{
						visual.find('li[data-value="'+this.value+'"]').addClass('active');
					});

					if(select.hasClass('select--multiple'))
					{
						var selectedTxt = select.attr('data-selected-txt') || 'Выбрано';


						if(selectedOptions.length > 1)
							value.html(selectedTxt + ' ' + selectedOptions.length);
						else
							value.html(selectedOptions.html());

						if(visual.hasClass('has-placeholder') && selectedOptions.length === 0)
							visual.addClass('placeholder-state');
					}
					else
					{
						value.html(visual.find('li[data-value="'+select.val()+'"]').html());
					}
				});

				body.off('keyup.accessibilityClick', '.js-select-visual, .js-select-visual li')
					 .on('keyup.accessibilityClick', '.js-select-visual, .js-select-visual li', function(e)
				{
					e.stopPropagation();
					if(e.which==13 || e.which==32)
						$(this).trigger('click');
				});

				body.off('click.closeSelects').on('click.closeSelects', function(e)
				{
					var target = $(e.target);

					if(target.closest('.js-select-visual, select').length === 0)
					{
						$('.js-select-visual.open').removeClass('open');
					}
				});


				body.off('updateSelect', 'select.js-select')
					 .on('updateSelect', 'select.js-select', function()
				{
					$(this).trigger('change.changeSelectVisual').trigger('recalcSelectAutoWidth');
				});
			},

			// подскролл
			anchor: function()
			{
				body.off('click.scrollToAnchor', '.js-anchor').on('click.scrollToAnchor', '.js-anchor', function(e)
				{
					var t = $(this);
					var target = t.attr('data-target');
          target = target ? $(target) : $(t.attr('href'));

          View.control.scrollToElement(target, {center: t.hasClass('js-anchor--center')});
				});
			},



			topMessages: function()
			{
				body.off('click.closeTopMessage').on('click.closeTopMessage', function()
				{
					View.control.closeTopMessages();
				});

				$(window).on('load.initTopMessages', function()
				{
					setTimeout(function()
					{
						$('.js-top-message').addClass('initialized');
					}, 5000);
				});
			},




			modals: function()
			{
				body.off('click.openModalByBtnClick', '.js-open-modal').on('click.openModalByBtnClick', '.js-open-modal', function()
				{
					var url = $(this).attr('data-modal-url');
					var selector = $(this).attr('data-modal');
					if(url)
						View.control.openModalByURL(url);
					else if(selector)
						View.control.openModal($(selector));
					else
						console.warn('openModal error: data-modal or data-modal-url not found');
				});

				body.off('click.closeModalByBtnClick', '.js-close-modal').on('click.closeModalByBtnClick', '.js-close-modal', function()
				{
					View.control.closeModal();
				});
			},




			// клик по затемнению
			overlay: function()
			{
				body.off('click.overlayClosesAll', '.js-overlay').on('click.overlayClosesAll', '.js-overlay', function()
				{
					View.control.closeAll();
				});


				body.off('click.closeAll', '.js-close-all').on('click.closeAll', '.js-close-all', function()
				{
					View.control.closeAll();
				});
			},



			escape: function () {
				$(document).keyup(function (e)
				{
					if (e.keyCode == 27)
					{
						View.control.closeAll();
						$('.js-select-visual.open').removeClass('open');
					}
				});
			},



			loadSvgSprite: function()
			{
        var meta = $('meta[name=sprite-atlas]', document.head);
        if (!meta || !meta[0])
          return;
        $.ajax({
          url: meta[0].content,
          method: 'GET',
          dataType: 'text'
        }).done(function(response){
          body.prepend(response);
        });
      },
		},


		// local - требует переинициализации после перезагрузки DOM
		local: {

			// инициализация часов
			flipClock: function(scope)
			{
				var clock;
				var timeInterval;

				$('.js-flip-clock', scope).each(function()
				{
					clock = $(this);
					timeInterval = Date.parse(clock.attr('data-counter-end')) - Date.now();
					clock.FlipClock(Math.round(timeInterval / 1000), {
						clockFace: 'DailyCounter',
						countdown: true,
						showSeconds: false,
						language: 'russian'
					});
          clock.on('click', function(e){ e.preventDefault(); return false; });
        });
			},



			// инициализация видео
			initVideo: function(scope)
			{
				$('.video-js', scope).each(function()
				{
					if (typeof window.videojs !== 'undefined')
						videojs(this, {}, function(){});
				});
			},



			// инициализация маски ввода для телефонов
			maskInput: function(scope)
			{
				$('.js-mask-input', scope).not('.is-stl').each(function()
				{
					$(this).inputmask({showMaskOnHover: false}).addClass('is-stl');
				});


				$(".js-mask-input--phone", scope).inputmask({mask: "+375 99 999-99-99", placeholder: '+375 __ ___-__-__', clearMaskOnLostFocus: true, showMaskOnHover: false});
				$(".js-mask-input--date", scope).inputmask({mask: "99.99.9999", placeholder: '__.__.____', clearMaskOnLostFocus: true, showMaskOnHover: false});
				$(".js-mask-input--passport-num", scope).inputmask({mask: "AA 9999999", placeholder: '__ _______', clearMaskOnLostFocus: true, showMaskOnHover: false});
				$(".js-mask-input--passport-id", scope).inputmask({mask: "9999999A999AA9", placeholder: '______________', clearMaskOnLostFocus: true, showMaskOnHover: false});

				$('.js-mask-input--units', scope).off('focus.clearMaskInputUnits').on('focus.clearMaskInputUnits', function()
				{
					if(!this.value)
						return;

					var result = this.value;
					var integer = $(this).attr('data-mask-integer') === 'true';

					result = result.toString()
								.replace(/[^0-9$.,]/g, '')
								.replace(/,/g, '.')
								.replace(/\s/g, "");

					if(integer)
						this.value = parseInt(result) || 0;
					else
						this.value = (parseFloat(result) || 0).toFixed(2).replace(/\./g, ',');
				});

				$('.js-mask-input--units', scope).off('blur.restoreMaskInputUnits').on('blur.restoreMaskInputUnits', function()
				{
					var result = this.value;
					var units = $(this).attr('data-mask-units');
					var integer = $(this).attr('data-mask-integer') === 'true';

					result = result.toString()
								.replace(/[^0-9$.,]/g, '')
								.replace(/,/g, '.')
								.replace(/\s/g, "");

					if(integer)
						this.value = (parseInt(result) || 0).toString().replace(/(\d)(?=(\d{3})+$)/g, '$1 ') + (units ? ' ' + units : '');
					else
						this.value = (parseFloat(result) || 0).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1 ').replace(/\./g, ',') + (units ? ' ' + units : '');
				});
			},



			// инпут со смещающимся плейсхолдером
			richTextInuptLocal: function(scope)
			{
				$('.js-rich-text-input_input', scope).trigger('change.markInputState');
			},



			// инициализация селектов
			selectsLocal: function(scope)
			{
				var selects = $(scope);
				if(!selects.is('select')) selects = $('select.js-select', scope);

				selects.each(function()
				{
					var select = $(this);
					var selectVisual = select.closest('.js-select-visual');
					var selectList;
					var selectChevron;
					var selectedText;
					var multiple = this.multiple;

					selectedText = select.find('option:selected');

					if(selectedText.length > 0) selectedText = selectedText.html();
					else selectedText = select.find('option:first-child').html();

					if(selectVisual.length)
					{
						selectVisual.children().not(select).remove();
						select.unwrap('.js-select-visual');
					}

					selectVisual = $('<div class="js-select-visual '+select.attr('class')+'" tabindex="0"></div>');
					selectValue = $('<span class="select-value js-select-value">'+selectedText+'</span>');
					selectChevron = $('<span class="select-chevron"></span>');
					selectList = $('<ul class="select-list"></ul>');

					var option;
					var optionContent;
					var optionClass;
					select.find('option').each(function()
					{
						option = $(this);
						optionContent = option.html();
						optionClass = 'class="';
						optionValue = option.attr('value');

						if(!optionValue)
							return;

						if(multiple)
							optionContent = '<span class="checkbox-row__visual"></span>' + optionContent;

						if(option.attr('value') === select.val())
							optionClass += 'active ';
						if(option.is(':disabled'))
							optionClass += 'disabled ';

						optionClass += '"';

						selectList.append(
							'<li data-value="'+optionValue+'" '+optionClass+'>' + optionContent + '</li>');
					});

					selectVisual.append(selectValue).append(selectChevron).append(selectList).removeClass('js-select js-alt-tab-controller js-rich-text-input__input');
					select.after(selectVisual);
					selectVisual.prepend(select);

					if(multiple)
					{
						select.addClass('select--multiple');
						selectVisual.addClass('select--multiple');
					}
				}).trigger('change.changeSelectVisual').trigger('recalcSelectAutoWidth');
			},
		}
	};



	this.initView = function()
	{
		body = $(document.body);
		htmlbody = $([document.documentElement, document.body]);
		win = $(window);
	}



	this.initAllGlobal = function()
	{
		$.each(this.init.global, function(index, fn)
		{
			if(typeof fn === 'function') fn();
		});
	};



	this.initAllLocal = function(scope)
	{
		$.each(this.init.local, function(index, fn)
		{
			if(typeof fn === 'function') fn(scope || document.body);
		});
	};
})();



function initAllLocal(event, scope)
{
	View.initAllLocal(scope);
}



function onPageReady()
{
	var body = $(document.body);

	body.addClass(View.mobileAndTabletCheck ? 'touch' : 'no-touch');
	body.addClass(View.isIE ? 'ie' : 'no-ie');
	body.addClass(navigator.userAgent.toLowerCase().indexOf('firefox') > -1 ? 'firefox' : 'no-firefox');

	View.initView();
	View.initAllGlobal();
	View.initAllLocal(body);

	if (window.location.hash){
		View.control.scrollToElement($(window.location.hash), {animate: false});
	}

	body.addClass('page-loaded');
}

// Kraft integration
$(document).ready(function(){
	if ($('#kraft-loader').length){
		// wait for krafter
		window.onKraftReady = onPageReady;
	} else {
		// ol plain page
    onPageReady();
  }
});