var loaderTag = document.getElementById('kraft-loader');
var baseZiegel = loaderTag.attributes['data-ziegel'].value;
var basePath = window.location.pathname.substr(0, window.location.pathname.lastIndexOf('/') + 1);
window.kraftRolled = true;

require.config({
  baseUrl: baseZiegel,
  paths: {
    Vue: 'assets/kraft/vue.min',
    videojs: 'assets/js/video.min'
  },
  timeout: 60
});

require(['Vue'], function(Vue){
  window.Vue = Vue;
  require(['Ziegels'], function(z){
    require(z, function(config){

      var app = new Vue({
        data: {
          slides: config.slides,
          rendered: false
        },

        render: function(h){
          if (!this.rendered){
            this.$nextTick(function(){
              this.$emit('rendered');
            });
            this.rendered = true;
          }
          return h('div', this.slides.map(function(e){
            var attrs = {};
            if (e.code){
              attrs['data-anchor'] = e.code;
              attrs['id'] = e.code;
            }
            return h(e.type, {props: e.props, attrs: attrs});
          }));
        }
      });

      app.$on('rendered', function(){
        if (typeof window.onKraftReady === 'function')
          window.onKraftReady();
      });
      app.$mount('#kraft-app');
    });
  })
});