define(['Vue', 'kraft-config'], function(Vue, config){
  var app = {
    data: {
      slides: config.slides,
      rendered: false,
      inspectable: false
    },

    render: function(h){
      if (!this.rendered){
        this.$nextTick(function(){
          this.$emit('rendered');
        });
        this.rendered = true;
      }
      return h('div', this.slides.map(function(e){
        return h(e.type, {
          props: e.props,
          // force recreate DOM
          key: e.id + '-' + Math.random(),
          // attr for subscrill
          attrs: {'data-slide-id': e.id}
        });
      }));
    }
  };

  if (window.parent && window.parent.app && window.parent.app.$model){
    app.data.inspectable = true;
    delete app.data.slides;

    app.computed = {
      slides: function(){
        return window.parent.app.$model.slides;
      }
    };

    app.mounted = function(){
      var self = this;
      window.parent.app.$root
        .$on('slide.render', function(){
          self.$nextTick(function(){
            self.$forceUpdate();
          });
        })
        .$on('slide.active', function(slide){
          var el = self.$el.querySelector('[data-slide-id="' + slide.id + '"]');
          if (!el)
            return;

          setTimeout(function(){
            window.scrollTo(0, el.offsetTop);
          }, 250);
        });
    }
  }

  return new Vue(app);
});