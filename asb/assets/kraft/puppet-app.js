var loaderTag = document.getElementById('kraft-loader');
var baseZiegel = loaderTag.attributes['data-ziegel'].value;
var basePath = window.location.pathname.substr(0, window.location.pathname.lastIndexOf('/') + 1);

require.config({
  baseUrl: baseZiegel,
  paths: {
    Vue: 'assets/kraft/vue.min',
    vue: 'assets/kraft/requirejs-vue',
    ziegel: 'ziegel',
    'kraft-app': 'assets/kraft/kraft-app',
    'kraft-config': basePath + 'app',
    'kraft-forms': basePath + '?kraft_api=forms.js'
  },
  timeout: 60
});

require(['Vue', 'ziegel', 'kraft-config', 'kraft-forms'], function(Vue, ziegel, config){
  window.Vue = Vue;

  var modules = [];
  for (i in ziegel.files.parts){
    modules.push('vue!' + ziegel.files.parts[i]);
  }

  require(modules, function(){
    require(['kraft-app'], function(app){
      app.$on('rendered', function(){
        if (typeof window.onKraftReady === 'function')
          window.onKraftReady();
      });
      app.$mount('#kraft-app');
    });
  });
});