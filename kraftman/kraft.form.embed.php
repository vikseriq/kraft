<?php
/**
 * [K]raftman
 *
 * Blazing fast landings web service.
 *
 * @copyright 20!8 vikseriq <vikseriq@gmail.com>
 * @license see license.md
 */

require_once __DIR__.'/vuer.php';

/**
 * Class KraftFormEmbed
 *
 * Embedded form generator
 */
class KraftFormEmbed {

	var $base_path;

	public function __construct(){
		$this->base_path = KraftConfig::forms_embed_path;
	}

	function out($data){
		if (!$data){
			header('404 Not Found', true, 404);
			echo 'Requested form not found';
			exit;
		}
		header('200 OK', true, 200);
		echo $data;
		exit;
		return false;
	}

	function load_form($form_id){
		$form_api = new KraftFormApi(KraftConfig::database_config);
		$form = $form_api->get_form($form_id);
		if (!$form)
			$this->out(false);

		return $form;
	}

	function load_base(){
		$file = $this->base_path.'/base.html';
		if (!file_exists($file))
			return $this->out(false);

		return file_get_contents($file);
	}

	function place_variables($template, $form){
		$data = $template;
		vuer::$loaderConfig['minimizeScript'] = true;

		// place form data
		$forms_data = json_encode($form, JSON_UNESCAPED_UNICODE);
		$form_data = '<script>(function(){window.kraftForm = '.$forms_data.';})();</script>';
		$data = str_replace('<!--%form%-->', $form_data, $data);

		// embed files
		preg_match_all('|<!--%embed:(.+)%-->|', $template, $matches);
		foreach ($matches[1] as $mi => $filename){
			$ext = substr($filename, strrpos($filename, '.'));
			$file = $this->base_path.'/'.$filename;
			if (!file_exists($file))
				continue;

			$file_content = file_get_contents($file);

			$data_part = '';
			switch ($ext){
				case '.js':
					$data_part = '<script>'.$file_content.'</script>';
					break;
				case '.css':
					$data_part = '<style>'.$file_content.'</style>';
					break;
				case '.vue':
					$data_part = vuer::load($file, true);
					break;
			}
			$data = str_replace($matches[0][$mi], $data_part, $data);
		}

		return $data;
	}

	public function process($params){
		if (empty($params['kraft_form']))
			$this->out(false);

		$form = $this->load_form($params['kraft_form']);
		// do not expose system configuration
		$form['meta'] = [
			'recaptcha_site' => arr::_($form['meta'], 'recaptcha_site')
		];
		$base_template = $this->load_base();

		$this->out($this->place_variables($base_template, $form));
	}
}