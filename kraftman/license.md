# [K]raftman, blazing fast landings and forms web-service.

Copyright @ 2018 vikseriq <vikseriq@gmail.com>

The unauthorized use of any part of this codebase is strictly prohibited and will lead to legal prosecution.

Unless required by applicable law or agreed to in writing, this software is distributed 
on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

Without limiting the foregoing, me (vikseriq) make __no warranty__ that:

* the software will meet your requirements;
* the software will be uninterrupted, timely, secure or error-free;
* the results that may be obtained from the use of the software will be effective, accurate or reliable;
* the quality of the software will meet your expectations;
* any errors in the software will be corrected for free.
