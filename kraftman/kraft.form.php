<?php
/**
 * [K]raftman
 *
 * Blazing fast landings web service.
 *
 * @copyright 20!8 vikseriq <vikseriq@gmail.com>
 * @license see license.md
 */

/**
 * Class KraftFormApi
 *
 * Form config and results provider
 */
class KraftFormApi {

	/**
	 * @var SQLite3 Database class
	 */
	var $db;
	var $db_config;
	const table_forms         = 'kraft_forms';
	const result_table_prefix = 'result_';
	const email_template      = KraftConfig::kraft_forms_path.'template.email.html';

	/**
	 * KraftFormApi constructor.
	 *
	 * Supported types:
	 *  SQLite 3
	 *
	 * @param $database_config string   Configuration in format "db_type;db_connection_path;db_password"
	 */
	public function __construct($database_config){
		$this->db_config = $database_config;
		$this->db = null;
	}

	/**
	 * Establishing a database connection
	 *
	 * @throws Exception on connection errors or unsupported type
	 */
	public function connect(){
		if ($this->db != null)
			return;
		$conf = explode(';', $this->db_config);
		$db_type = $conf[0];
		$db_path = $conf[1];
		switch ($db_type){
			case 'sqlite3':
				if (file_exists($db_path)){
					$this->db = new SQLite3($db_path, SQLITE3_OPEN_READWRITE, $conf[2]);
				}
				break;
			default:
				throw new Exception('database_unknown');
		}
		if (!$this->db)
			throw new Exception('database_no_access');
	}

	/**
	 * Return list of all forms with exploded `fields` (array) and `meta` (object)
	 * @return array
	 * @throws Exception
	 */
	public function forms_list(){
		$this->connect();

		$data = [];
		$ob = $this->db->query("SELECT * FROM `".self::table_forms."`");
		while ($ar = $ob->fetchArray(SQLITE3_ASSOC)){
			$ar['fields'] = json_decode($ar['fields'], true);
			if (!$ar['fields'])
				$ar['fields'] = [];
			$ar['meta'] = json_decode($ar['meta'], false);
			if (!$ar['meta'])
				$ar['meta'] = new stdClass();
			$data[$ar['id']] = $ar;
		};

		return $data;
	}

	/**
	 * Escape string for SQL queries
	 *
	 * @param $string
	 * @return string
	 */
	static function esc($string){
		return SQLite3::escapeString($string);
	}

	/**
	 * Updating form configuration
	 *
	 * @param $form_data
	 * @return int
	 * @throws Exception
	 */
	public function update_form($form_data){
		$this->connect();

		$ob = $this->db->querySingle("SELECT id FROM `".self::table_forms."`"
			." WHERE id = '".self::esc($form_data['id'])."'");
		if (!$ob){
			$this->db->query(sprintf(
				"INSERT INTO `%s` (id, code, name, fields, meta) VALUES ('%s', '%s', '%s', '%s', '%s')",
				self::table_forms,
				self::esc($form_data['id']),
				self::esc($form_data['code']),
				self::esc($form_data['name']),
				'[]', '{}'
			));
			return $this->db->lastInsertRowID();
		} else {
			$fields = json_encode($form_data['fields'], JSON_UNESCAPED_UNICODE);
			if (!$fields)
				$fields = '[]';
			$meta = json_encode($form_data['meta'], JSON_UNESCAPED_UNICODE);
			if (!$meta)
				$meta = '{}';

			$this->db->query(sprintf(
				"UPDATE `%s` SET code = '%s', name = '%s', fields = '%s', meta = '%s' WHERE id = '%s'",
				self::table_forms,
				self::esc($form_data['code']),
				self::esc($form_data['name']),
				self::esc($fields),
				self::esc($meta),
				self::esc($form_data['id'])
			));
		}
		return $this->db->changes();
	}

	/**
	 * Returns form configuration by form id
	 *
	 * @param $form_id
	 * @return mixed|null
	 * @throws Exception
	 */
	public function get_form($form_id){
		$forms = $this->forms_list();
		if (isset($forms[$form_id]))
			return $forms[$form_id];
		return null;
	}

	/**
	 * Return result tables list: tables with prefix `self::result_table_prefix`
	 *
	 * @return array
	 * @throws Exception
	 */
	public function tables_list(){
		$this->connect();

		$data = [];
		$ob = $this->db->query("SELECT `name` FROM `sqlite_master` WHERE `type` = 'table' AND `name` LIKE '".self::result_table_prefix."%'");
		while ($ar = $ob->fetchArray(SQLITE3_ASSOC))
			$data[] = $ar['name'];

		return $data;
	}

	/**
	 * Return results from `$table_name` for form `$form_id` with `$nav` pagination
	 *
	 * @param $table_name string    Result table name
	 * @param $form_id string       Form id
	 * @param $nav array            Navigation params: 'offset' - to start from, 'limit' - count max results to return
	 * @return array                Form results with exploded `values` (object) and `meta` (object)
	 * @throws Exception
	 */
	public function result_list($table_name, $form_id, $nav = []){
		$tables = $this->tables_list();
		if (!in_array($table_name, $tables, true))
			throw new Exception('wrong_table');

		$data = [];
		$limitstart = isset($nav['offset']) ? abs($nav['offset']) : 0;
		$limit = isset($nav['limit']) ? abs($nav['limit']) : 5000;

		$ob = $this->db->query(sprintf(
			"SELECT * FROM `%s` WHERE form_id = '%s' ORDER BY created DESC LIMIT %d, %d",
			$table_name,
			self::esc($form_id),
			$limitstart, $limit
		));

		while ($ar = $ob->fetchArray(SQLITE3_ASSOC)){
			$ar['values'] = json_decode($ar['values'], false);
			if (!$ar['values'])
				$ar['values'] = new stdClass();
			$ar['meta'] = json_decode($ar['meta'], false);
			if (!$ar['meta'])
				$ar['meta'] = new stdClass();

			$data[] = $ar;
		}

		return $data;
	}

	/**
	 * List external directory value files
	 *
	 * @return array
	 */
	public function externaldirectory_list(){
		$data = [];
		$files = scandir(KraftConfig::kraft_forms_path);

		foreach ($files as $f){
			if (substr(strtolower($f), -4) === '.csv')
				$data[] = $f;
		}

		return $data;
	}

	/**
	 * Load CSV as directory array
	 *
	 * First line - schema
	 *
	 * @param $directory
	 * @return array
	 * @throws Exception
	 */
	public function load_directory($directory){
		$directories = $this->externaldirectory_list();
		if (!in_array($directory, $directories))
			throw new Exception('directory_unknown');

		$data = [];
		$scheme = [];
		$scheme_row = [];

		$filename = KraftConfig::kraft_forms_path.$directory;
		$f = fopen($filename, 'r');
		$line_i = 0;
		// read csv file
		while (!feof($f)){
			$line = fgetcsv($f);
			$line_i++;

			if (!$scheme){
				// first line - scheme
				$scheme = $line;
				// force first column as id
				if ($line[0] !== 'id')
					array_unshift($line, 'id');
				$scheme_row = $line;
			} else {
				// fill non empty columns as declared in scheme
				$row = [];
				$key = $line_i;
				foreach ($scheme as $i => $col){
					if ($col === 'id')
						$key = $line[$i];
					else
						$row[] = trim($line[$i]);
				}
				array_unshift($row, $key.'');

				// strip empty
				if (!strlen(implode($row)))
					continue;

				$data[] = $row;
			}
		};
		fclose($f);

		return ['scheme' => $scheme_row, 'items' => $data];
	}

	/**
	 * Loads externalDirectory values
	 *
	 * @param $directory
	 * @return array
	 * @throws Exception
	 */
	public function externaldirectory_item($directory){
		return $this->load_directory($directory);
	}

	/**
	 * Send form results to email
	 *
	 * Email template file format
	 * line 1: email message subject
	 * line 2: email format: text/html
	 * line 3: value row template
	 * line 4: separator, will be stripped
	 * line 5 and next: message body
	 *
	 * @param $to string        Admin email
	 * @param $form array       Form data
	 * @param $result array     Result data
	 * @return bool
	 */
	public function send_email($to, $form, $result){
		// load mail template
		if (!file_exists(self::email_template))
			return false;

		$email_template = file_get_contents(self::email_template);
		$_parts = explode(PHP_EOL, $email_template, 5);
		$email_header = trim($_parts[0]);
		$email_format_html = strcasecmp(trim($_parts[1]), 'html') === 0;
		$value_row_template = trim($_parts[2]);
		$message = trim($_parts[4]);

		// fill template
		$rules = [
			[
				'#form_name#', '#form_code#',
				'#date#', '#time#', '#datetime#',
				'#href#', '#ip#'
			],
			[
				$form['name'], $form['code'],
				date('d.m.Y', $result['created']),
				date('H:i:s', $result['created']),
				date('Y-m-d H:i:s', $result['created']),
				$result['meta']['href'],
				$result['meta']['ip']
			]
		];

		$valuetext = [];
		foreach ($result['values'] as $key => $value){
			$rules[0][] = '#value.'.$key.'#';
			$rules[1][] = $value;

			// collect values
			$label = $key;
			foreach ($form['fields'] as $f){
				if ($f['code'] == $key){
					$label = $f['name'];
					break;
				}
			}
			// value with label as formatted string
			$valuetext[] = str_replace(
				['#key#', '#label#', '#value#'],
				[$key, $label, $value],
				$value_row_template
			);
		}
		$rules[0][] = '#values#';
		$rules[1][] = implode(PHP_EOL, $valuetext);

		$message = str_replace($rules[0], $rules[1], $message);
		$email_header = str_replace($rules[0], $rules[1], $email_header);

		$headers = [
			'X-Mailer: PHP/'.phpversion().' Kraft'
		];
		if ($email_format_html){
			$headers[] = 'MIME-Version: 1.0';
			$headers[] = 'Content-type: text/html; charset=utf-8';
		}

		$sent = mail($to, $email_header, $message, implode("\r\n", $headers));
		return $sent;
	}

	/**
	 * Exports form results as csv to a file or browser
	 *
	 * @param $form_id
	 * @param $table
	 * @param $format
	 * @throws Exception
	 */
	public function export_file($form_id, $table, $format, $output_file = ''){
		if ($format != 'csv')
			throw new Exception('unsupported_format');

		$forms = $this->forms_list();
		if (!isset($forms[$form_id]))
			throw new Exception('form_not_exists');
		$form = $forms[$form_id];
		$out_name = 'form_'.$form['code'].'_'.date('Y-m-d_H-i').'.csv';

		if (!$output_file){
			// write csv header
			header('Content-Type: text/csv; utf-8');
			header('Content-Disposition: attachment; filename='.$out_name);
			header('Pragma: no-cache');

			$out = fopen('php://output', 'w');
		} else {
			$out = fopen($output_file, 'w');
		}

		$row = ['id', 'created'];
		foreach ($form['fields'] as $f){
			$row[] = $f['name'];
		}
		fputcsv($out, $row, ';');

		$results = $this->result_list($table, $form_id);
		foreach ($results as $result){
			$row = [$result['id'], date('c', $result['created'])];
			foreach ($form['fields'] as $f){
				if (isset($result['values']->{$f['code']}))
					$row[] = $result['values']->{$f['code']};
				else
					$row[] = '';
			}
			fputcsv($out, $row, ';');
		}

		fclose($out);
	}

	/**
	 * Validate Google reCAPTCHA input
	 *
	 * @param $recaptcha_secret
	 * @param $user_response
	 * @return bool
	 */
	public function validate_captcha($recaptcha_secret, $user_response){

		$context = stream_context_create([
			'http' => [
				'method' => 'POST',
				'header' => 'Content-type: application/x-www-form-urlencoded',
				'content' => http_build_query([
					'secret' => $recaptcha_secret,
					'response' => $user_response
				])
			]
		]);

		$raw = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
		if (strlen($raw)){
			$out = json_decode($raw, 1);
			if ($out['success'])
				return true;
			else
				return implode(';', arr::_($out, 'error-codes', []));
		}
		return false;
	}

	/**
	 * Write form result: store to database and/or send to email
	 *
	 * @param $form_id
	 * @param $data
	 * @param $meta
	 * @return bool
	 * @throws Exception
	 */
	public function result_save($form_id, $data, $meta){
		$forms = $this->forms_list();
		if (!isset($forms[$form_id]))
			throw new Exception('form_not_exists');
		$form = $forms[$form_id];

		$time = time();
		$json_data = json_encode($data, JSON_UNESCAPED_UNICODE);
		$json_meta = json_encode($meta, JSON_UNESCAPED_UNICODE);

		$result_data = [
			'values' => $data,
			'meta' => $meta,
			'created' => $time
		];

		// captcha check
		$not_spam = true;
		if ($captcha_key = arr::_($form['meta'], 'recaptcha_secret')){
			$not_spam = false;
			if ($captcha_input = arr::_($data, 'captcha')){
				$captcha_result = $this->validate_captcha($captcha_key, $captcha_input);
				if ($captcha_result === true)
					$not_spam = true;
				else throw new Exception('reject: '.$captcha_result);
			}
		}

		$ok = true;
		// save to storage
		if ($not_spam && !empty($form['meta']->table)){
			$ok &= $this->db->query(sprintf(
				"INSERT INTO `%s` (form_id, `values`, meta, created) VALUES ('%s', '%s', '%s', %d)",
				$form['meta']->table,
				self::esc($form_id),
				self::esc($json_data),
				self::esc($json_meta),
				$time
			));
		}

		// email notification
		if ($not_spam && !empty($form['meta']->mailto)){
			$ok &= $this->send_email($form['meta']->mailto, $form, $result_data);
		}

		return $ok ? true : false;
	}

}
