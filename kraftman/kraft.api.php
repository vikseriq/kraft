<?php
/**
 * [K]raftman
 *
 * Blazing fast landings web service.
 *
 * @copyright 20!8 vikseriq <vikseriq@gmail.com>
 * @license see license.md
 */

/**
 * Class KraftApi
 *
 * REST-like API for managing [k]raft creatures
 */
class KraftApi {

	var $creatures_path, $storage_path;
	var $start_time, $action;
	var $form_api;

	public function __construct($creatures_path){
		$this->creatures_path = $creatures_path;
		$this->storage_path = __DIR__;

		$db_config = KraftConfig::database_config;
		$this->form_api = new KraftFormApi($db_config);
	}

	public function json_reply($error = null, $data = []){
		$out = [
			'error' => $error ? $error : false,
			'action' => $this->action,
			'time' => sprintf('%.2f s.', microtime(1) - $this->start_time),
			'data' => $data,
		];

		ob_end_clean();
		header('Content-Type: application/json; charset=utf-8');
		ob_start('ob_gzhandler');
		echo json_encode($out, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
		ob_end_flush();
		exit;
		return false;
	}

	public function handle(){
		$this->start_time = microtime(1);

		$input = file_get_contents('php://input');
		$data = json_decode($input, true);
		if (empty($data))
			$data = $_POST;
		if (empty($data))
			$data = $_GET;

		if ($data && !empty($data['action'])){
			$this->action = $data['action'];
			$method_name = 'action_'.strtolower(str_replace('.', '_', $data['action']));
			try {
				if (method_exists($this, $method_name)){
					return $this->$method_name($data);
				}
			} catch (Exception $err) {
				return $this->json_reply($err->getMessage());
			}
		} elseif (!empty($_GET['kraft_api']) || !empty($_POST['kraft_api'])) {
			$this->action = $_REQUEST['kraft_api'];
			$method_name = 'action_api_'.strtolower(str_replace('.', '_', $this->action));
			try {
				if (method_exists($this, $method_name)){
					return $this->$method_name($data);
				}
			} catch (Exception $err) {
				return $this->json_reply($err->getMessage());
			}
		}

		return $this->json_reply('no_action');
	}

	function action_creature_save($data){
		$slides = $data['slides'];
		$creature = $data['creature'];
		if (empty($creature) || !is_array($creature) || !is_array($slides))
			return $this->json_reply('bad_data');

		$config_file = $this->creature_file_by_id($data['creature']['id']);
		if (!file_exists($config_file))
			return $this->json_reply('not_found');

		$creature['updated'] = date('c');
		$app_config = [
			'creature' => $creature,
			'slides' => $slides
		];

		file_put_contents($config_file, $this->creature_js_config($app_config));

		return $this->json_reply(false, 'success');
	}

	function creature_file($name){
		return str_replace('//', '/', $this->creatures_path.'/'.$name.'/app.js');
	}

	function creature_dir($name){
		return str_replace('//', '/', $this->creatures_path.'/'.$name.'/');
	}

	function creature_js_config($values){
		$data = array_merge([
			'creature' => [],
			'slides' => []
		], $values);

		$json = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

		return 'define(function(){return '.$json.PHP_EOL.'});';
	}

	function creature_init($dir, $config){
		if (!mkdir($dir))
			return false;
		file_put_contents($dir.'/app.js', $this->creature_js_config($config));
		file_put_contents($dir.'/index.php', '<'.'?php define("KRAFT_ENTRY", __DIR__); include_once __DIR__."/../kraft.php"; ?'.'>');
		return true;
	}

	function creature_file_by_id($id){
		$creatures = $this->get_creatures();
		$creature_file = null;
		foreach ($creatures as $file => $c){
			if ($c['id'] == $id){
				$creature_file = $file;
				break;
			}
		}
		return $creature_file;
	}

	static function read_creature($file){
		$config = [];
		if (file_exists($file)){
			$data = file_get_contents($file);
			if (preg_match('|"creature":\s*(\{.*?\})|ms', $data, $m)){
				$config = json_decode($m[1], true);
			}
		}
		return $config;
	}

	function get_creatures(){
		$files = scandir($this->creatures_path);
		$configs = [];

		foreach ($files as $file){
			if ($file[0] == '.')
				continue;
			$app_config = $this->creature_file($file);
			if ($conf = self::read_creature($app_config)){
				$configs[$app_config] = $conf;
				$configs[$app_config]['server_path'] = '/'.$file.'/';
			}
		}

		return $configs;
	}

	function _unlink($path){
		if (is_file($path))
			return unlink($path);
		else {
			foreach (glob($path.'/*') as $file)
				$this->_unlink($file);
			return rmdir($path);
		}
	}

	function action_creature_list(){
		$configs = array_values($this->get_creatures());

		return $this->json_reply(false, $configs);
	}

	function action_creature_get($data){
		if (empty($data['id']))
			return $this->json_reply('bad_data');

		$creature = $this->creature_file_by_id($data['id']);
		if (!$creature)
			return $this->json_reply('bad_data');

		$data = file_get_contents($creature);

		return $this->json_reply(false, $data);
	}

	function action_creature_update($data){
		if (empty($data['data']))
			return $this->json_reply('bad_data');
		$data = $data['data'];

		$configs = $this->get_creatures();
		$originalPath = preg_replace('/[^a-z0-9-_]+/', '', $data['originalPath']);
		$path = preg_replace('/[^a-z0-9-_]+/', '', $data['path']);
		if (!$originalPath){
			// create new
			$newdir = $this->creature_dir($path);
			if (file_exists($newdir))
				return $this->json_reply('already_exists');

			if (!$this->creature_init($newdir, [
				'creature' => [
					'id' => $data['id'],
					'web_path' => $path,
					'name' => $data['name'],
					'created' => date('c'),
					'ziegel' => $data['ziegel']
				]
			]))
				return $this->json_reply('create_fail');

		} else {
			// existing
			$key = $this->creature_file($originalPath);
			if (!isset($configs[$key]))
				return $this->json_reply('not_found '.$key);
			if ($configs[$key]['id'] != $data['id'])
				return $this->json_reply('invalid_id');

			$dir = dirname($key);
			if ($path){
				// move
				$newdir = $this->creature_dir($path);
				if (file_exists($newdir))
					return $this->json_reply('already_exists');

				rename($dir, $newdir);
			} else {
				// remove
				$files = scandir($dir);
				if (count($files) <= 5)
					if (!$this->_unlink($dir))
						return $this->json_reply('remove_fail');
			}
		}

		return $this->json_reply(false, 'success');
	}

	public static function read_ziegel($file){
		$config = [];
		if (file_exists($file)){
			$data = file_get_contents($file);
			if (preg_match('|return\s+({.*?})|ms', $data, $m)){
				$json = json_decode($m[1], true);
				// strange greedy-related match bug
				if (!$json)
					$json = json_decode($m[1].'}', true);

				if ($json)
					$config = $json;
			}
		}
		return $config;
	}

	function get_ziegels(){
		$files = scandir($this->creatures_path);
		$configs = [];

		foreach ($files as $file){
			if ($file[0] == '.')
				continue;
			$file_config = $this->creatures_path.'/'.$file.'/ziegel.js';
			if ($conf = self::read_ziegel($file_config)){
				$configs[$file_config] = $conf;
				$configs[$file_config]['server_path'] = $file;
			}
		}

		return $configs;
	}

	function action_ziegels_list(){
		$configs = array_values($this->get_ziegels());

		return $this->json_reply(false, $configs);
	}

	function action_form_list(){
		return $this->json_reply(false, array_values($this->form_api->forms_list()));
	}

	function action_form_update($data){
		if (empty($data['data']) || empty($data['data']['id']))
			return $this->json_reply('bad_data');

		$result = $this->form_api->update_form($data['data']);

		return $this->json_reply(false, 'success;'.$result);
	}

	function action_form_table_list(){
		return $this->json_reply(false, array_values($this->form_api->tables_list()));
	}

	function action_form_result_list($data){
		if (empty($data['form_id']) || empty($data['table']))
			return $this->json_reply('bad_data');

		$results = $this->form_api->result_list($data['table'], $data['form_id']);
		return $this->json_reply(false, $results);
	}

	function action_form_externaldirectory_list(){
		return $this->json_reply(false, array_values($this->form_api->externaldirectory_list()));
	}

	public static function forms_amd($form_ids = []){
		$instance = new KraftApi(__DIR__);
		$forms = $instance->form_api->forms_list();
		if (!empty($form_ids)){
			foreach ($forms as $form_id => $_){
				if (in_array($form_id, $form_ids))
					unset($forms[$form_id]);
			}
		}
		$forms_data = json_encode($forms, JSON_UNESCAPED_UNICODE);
		return 'define("kraft-forms", function(){window.kraftForms = '.$forms_data.';});';
	}

	function action_api_forms_js(){
		echo self::forms_amd();
		exit;
	}

	function action_api_external($data){
		if (empty($data['external_directory']))
			$this->json_reply(true);

		$this->json_reply(false, $this->form_api->externaldirectory_item($data['external_directory']));
	}

	function action_api_antrag($data){
		if (empty($data['form_id']) || count($data) < 2)
			$this->json_reply(true);

		$form_id = $data['form_id'];
		$meta = [
			'ip' => $_SERVER['REMOTE_ADDR'],
			'href' => $data['form_href']
		];
		unset($data['form_id'], $data['form_href']);

		$ok = $this->form_api->result_save($form_id, $data, $meta);
		$this->json_reply(false, $ok ? 'success' : 'warning');
	}

	function action_api_form_result_export($data){
		if (empty($data) || empty($data['form_id']))
			$this->json_reply('bad_data');

		$this->form_api->export_file($data['form_id'], $data['table'], $data['format']);
		exit;
	}

}

class arr {

	static function _($array, $key, $default = null){
		if (is_array($array))
			return isset($array[$key]) ? $array[$key] : $default;
		if (is_object($array))
			return property_exists($array, $key) ? $array->$key : $default;
		return $default;
	}

}