define(['Vue'], function(Vue){

  var KraftApi = {
    base: '/kraft.php'
  };

  var ZiegelType = function(args){
    var self = this;
    _.each(arguments, function(e){
      _.extend(self, e);
    })
  };

  ZiegelType.prototype.props = [];
  ZiegelType.prototype.name = '';
  ZiegelType.prototype.sort = 100;

  ZiegelType.prototype.compareTo = function(other){
    var x = this.sort - other.sort;
    if (!x)
      x = this.name.toLocaleLowerCase().localeCompare(other.name.toLocaleLowerCase());
    return x;
  };

  return new Vue({
    data: {
      creature: {
        id: '',
        name: '',
        path: '',
        server_path: '',
        ziegel: '',
        htmlMeta: '',
        htmlBody: ''
      },
      creature_path: '',
      slides: [],
      ziegel: {},
      ziegelTypes: [],
      ziegels: [],
      creatures: [],
      forms: [],
      formTables: [],
      externalDirectory: []
    },

    watch: {
      slides: {
        handler: function(){
          this.sync()
        },
        deep: true
      }
    },

    methods: {
      sync: _.throttle(function(object){
        this.$emit('slide.render');
      }, 500),

      loadCreature: function(id){
        var self = this;
        self.fetchCreatures()
          .then(function(creatures){
            self.creature = _.find(creatures, {id: id});
            self.creature_path = self.creature.server_path;
          })
          .then(self.listZiegels)
          .then(function(ziegels){
            // clean up
            self.ziegel = _.find(ziegels, {id: self.creature.ziegel});
            self.slides.splice(0);
            self.ziegelTypes.splice(0);
          })
          .then(function(){
            // load creature
            require([self.creature_path + '/app.js'], function(config_static){
              var config = JSON.parse(JSON.stringify(config_static));
              self.$set(self.$data, 'creature', config.creature);
              self.$set(self.$data, 'slides', config.slides);

              // setup ziegel modules
              self.loadZiegel(function(){
                // done
                self.$emit('creature.loaded');
              })
            });
          })
          .then(self.fetchForms)
      },

      addSlide: function(){
        this.slides.push({
          id: _.uid(),
          type: '_blank',
          title: 'Этаж',
          code: '',
          props: []
        });

        this.sync();
      },

      rootPath: function(){
        var path = window.location.pathname.split('/');
        path = path.slice(0, -2).join('/') + '/';
        return path;
      },

      ziegelBase: function(){
        return (this.rootPath() + this.creature_path + '/index.php').replace(/\/\//g, '\/');
      },

      /**
       * Loading ziegel's Vue modules via vuer loader
       * @param callback
       */
      loadZiegel: function(callback){
        var self = this;
        var path = this.rootPath() + this.ziegel.files.path;
        var modules = _.map(this.ziegel.files.parts, function(e){
          return 'vue!' + path + e;
        });

        require(modules, function(){
          self.fillZiegelTypes();
          callback();
        });
      },

      /**
       * Populate ziegel list with actual types
       */
      fillZiegelTypes: function(){
        var self = this;

        // extracting names
        var fileRegex = new RegExp('.*\/(.*)\.(vue|html)$');
        var componentNames = _.map(this.ziegel.files.parts, function(e){
          var m = fileRegex.exec(e);
          if (m && m[1]){
            return m[1];
          }
          return false;
        });

        // extracting component meta by names
        _.each(componentNames, function(name){
          if (name && Vue.component(name) && Vue.component(name).options.kraft){
            self.ziegelTypes.push(new ZiegelType({id: name}, Vue.component(name).options.kraft));
          }
        });

        self.ziegelTypes.push(new ZiegelType({
          id: '_blank',
          name: 'Пустой этаж',
          sort: 10
        }));

        self.ziegelTypes.sort(function(a, b){
          return a.compareTo(b);
        })
      },

      fetchData: function(body){
        var self = this;
        return new Promise(function(done, fail){
          fetch(KraftApi.base, {
            method: 'post',
            body: JSON.stringify(body),
            headers: {
              "Content-Type": "application/json"
            },
            credentials: "same-origin"
          }).then(function(response){
            try {
              response.json().then(function(data){
                if (data.error)
                  fail(data.error);
                else
                  done(data.data);
              }, fail);
            } catch (e) {
              fail();
            }
          }, fail);
        });
      },

      listZiegels: function(){
        var self = this;
        return new Promise(function(done, fail){
          if (self.ziegels.length)
            return done(self.ziegels);

          return self.fetchData({
            action: 'ziegels.list'
          }).then(function(ziegels){
            self.ziegels.splice(0);
            _.each(ziegels, function(z){
              self.ziegels.push(z);
            });
            done(self.ziegels);
          }, fail)
        })
      },

      fetchCreatures: function(){
        var self = this;
        return new Promise(function(done, fail){
          if (self.creatures.length)
            return done(self.creatures);

          self.creatures.splice(0);
          return self.fetchData({
            action: 'creature.list'
          }).then(function(creatures){
            _.each(creatures, function(c){
              self.creatures.push(c);
            });
            done(self.creatures);
          }, fail);
        });
      },

      updateCreature: function(data){
        return new Promise(function(done, fail){
          fetch(KraftApi.base, {
            method: 'post',
            body: JSON.stringify({
              action: 'creature.update',
              data: data
            })
          }).then(function(response){
            if (response.status < 400){
              response.json().then(function(data){
                if (data.error)
                  fail(data.error);
                else
                  done();
              });
            } else fail();
          }, fail);
        });
      },

      saveCreature: function(){
        var self = this;
        return new Promise(function(done, fail){
          self.fetchData({
            action: 'creature.save',
            slides: self.slides,
            creature: self.creature
          }).then(done, fail);
        });
      },

      fetchForms: function(){
        var self = this;
        return new Promise(function(done, fail){
          self.fetchData({
            action: 'form.list'
          }).then(function(data){
            self.forms = data;
            done(self.forms);
          }, fail);
        });
      },

      updateForm: function(data){
        var self = this;
        if (data && data.fields){
          data.fields = _.without(data.fields, {code: ''});
        }
        return new Promise(function(done, fail){
          self.fetchData({
            action: 'form.update',
            data: data
          }).then(done, function(reason){
            fail(reason || 'Создание форм временно недоступно');
          });
        })
      },

      fetchFormTables: function(){
        var self = this;
        return new Promise(function(done, fail){
          if (self.formTables.length)
            return done(self.formTables);

          self.fetchData({
            action: 'form.table.list'
          }).then(function(data){
            self.formTables = data;
            done(self.formTables);
          }, fail);
        });
      },

      fetchExternalDirectory: function(){
        var self = this;
        return new Promise(function(done, fail){
          if (self.externalDirectory.length)
            return done(self.externalDirectory);

          self.fetchData({
            action: 'form.externaldirectory.list'
          }).then(function(data){
            self.externalDirectory = data;
            done(self.externalDirectory);
          }, fail);
        });
      },

      loadAntrage: function(data){
        var self = this;
        return new Promise(function(done, fail){
          if (!data || !data.form_id)
            return fail('bad_params ');

          self.fetchData({
            action: 'form.result.list',
            table: data.table,
            form_id: data.form_id
          }).then(function(data){
            done(data);
          }, fail);
        });
      },

      exportLink: function(data){
        switch (data.type){
          case 'antrage':
            return KraftApi.base
              + '?kraft_api=form_result_export'
              + '&form_id=' + data.form_id
              + '&table=' + data.table
              + '&format=' + data.format;
          default:
            return '#';
        }
      }
    }
  });
});