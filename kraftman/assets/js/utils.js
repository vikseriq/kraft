define(['underscore'], function(_){

  // underscore.path
  var _underscore_path = {
    path: function(context, key){
      key = key || '';
      key = key.replace(/\[/g, '.').replace(/(\[|\])/g, '');
      var paths = key.split('.');
      if (!paths.length || !context)
        return undefined;
      var object = context[paths.shift()];

      _.each(paths, function(key){
        if (object)
          object = object[key];
      });

      return object;
    }
  };

  _.mixin(_underscore_path);

  // underscore.uid
  _.mixin({
    uid: function(length){
      return (Math.random() / new Date() * Math.pow(10, 12)).toString(36).substr(2, (length || 10))
    }
  });

});