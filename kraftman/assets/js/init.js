require.config({
  baseUrl: './assets/js',
  paths: {
    Vue: 'libs/vue',
    Split: 'libs/split.min',
    vue: 'libs/requirejs-vue',
    VueRouter: 'libs/vue-router.min',
    underscore: 'libs/underscore-min'
  },
  shim: {
    '_': 'underscore'
  },
  timeout: 60,
  urlArgs: 'mt=' + +(new Date())
});

require(['Vue', 'libs/vuetify', 'VueRouter', 'underscore'], function(Vue, Vuetify, VueRouter, _){
  Vue.config.devtools = false;
  Vue.config.productionTip = false;
  Vue.use(VueRouter);
  if (typeof window !== 'undefined'){
    Vuetify.default(Vue);
    window.Vue = Vue;
    window._ = _;
  }

  require(['model', 'state', 'utils'], function(){
    Vue.prototype.$model = require('model');
    Vue.prototype.$state = require('state');

    require(['vue!werft'], function(app){
      window.app = app;
      app.$mount('#werft');
    });
  })

});