define(['Vue'], function(Vue){
  return new Vue({
    data: {
      user: {
        auth: false
      }
    },

    methods: {
      checkAuth: function(user){
        if (!user){
          var user_data = localStorage.getItem('user');
          user = _.extend({login: '', password: ''}, JSON.parse(user_data))
        }
        return new Promise(function(done, fail){
          if (user.login === user.password && user.login === 'admin'){
            localStorage.setItem('user', JSON.stringify(user));
            return done();
          }
          fail();
        })
      },

      logOff: function(){
        localStorage.removeItem('user');
      }
    }
  })
});