<?php
/**
 * [K]raftman
 *
 * Blazing fast landings web service.
 *
 * @copyright 20!8 vikseriq <vikseriq@gmail.com>
 * @license see license.md
 */

require_once __DIR__.'/vuer.php';

/**
 * Class KraftBuilder
 *
 * Landing page builder from creature and ziegels
 */
class KraftBuilder {

	var $entry_path;
	var $base_path;
	var $env_bundled;

	public function __construct($entry, $base){
		$this->entry_path = $entry;
		$this->base_path = $base;
		$this->env_bundled = empty($_GET['kraftman']);
	}

	function out($data){
		if (!$data){
			header('404 Not Found', true, 404);
			exit;
		}
		header('200 OK', true, 200);
		echo $data;
		exit;
		return true;
	}

	function read_config($path){
		$config = KraftApi::read_creature($path.'/app.js');
		if (!$config)
			return $this->out(false);

		return $config;
	}

	function load_ziegel($ziegel_code){
		$ziegel = KraftApi::read_ziegel($this->base_path.'/'.$ziegel_code.'/ziegel.js');
		if (!$ziegel)
			return $this->out(false);

		return $ziegel;
	}

	function load_ziegel_base($ziegel){
		$file = $this->base_path.'/'.$ziegel['files']['path'].'/'.$ziegel['files']['base'];
		if (!file_exists($file))
			return $this->out(false);

		return file_get_contents($file);
	}

	function place_variables($template, $config, $ziegel){
		$data = str_replace(
			[
				'%base%',
				'<!--%htmlMeta%-->',
				'<!--%htmlBody%-->',
			],
			[
				'/../'.$ziegel['files']['path'],
				$config['htmlMeta'],
				$config['htmlBody']
			],
			$template
		);

		$data = str_replace(
			[
				'%htmlTitle%',
				'%appType%'
			],
			[
				$config['htmlTitle'],
				$this->env_bundled ? 'rolled-app' : 'puppet-app'
			],
			$data
		);

		if ($this->env_bundled){
			$app_config = file_get_contents($this->entry_path.'/app.js');
			$app_config = str_replace('define(function', 'define("kraft-config", function', $app_config);

			$forms_props = KraftApi::forms_amd();

			$ziegels = [
				'kraft-config' => vuer::whitespaces($app_config),
				'kraft-forms' => vuer::whitespaces($forms_props)
			];
			vuer::$loaderConfig['minimizeScript'] = true;
			foreach ($ziegel['files']['parts'] as $part){
				$vue = vuer::load($this->base_path.'/'.$ziegel['files']['path'].'/'.$part, false)['script'];
				// removing 'kraft'-object: frontend does not need this
				$p = strpos($vue, 'kraft: {');
				if ($p){
					$start = $p;
					$i = $p;
					$n = mb_strlen($vue, 'windows-1251');
					$brackets = 0;
					while ($vue[$i] != '{')
						$i++;
					do {
						if ($vue[$i] === '{'){
							$brackets++;
						} elseif ($vue[$i] === '}') {
							$brackets--;
						}
						$i++;
					} while ($i < $n && $brackets > 0);

					$vue = mb_substr($vue, 0, $start, 'windows-1251')
						.'k: "'.$start.' '.$i.'"'
						.mb_substr($vue, $i, null, 'windows-1251');
				}

				// generate unique id for instances
				$id = 'ziegel_'.rand(1000, 9999);
				$vue = str_replace('define([', 'define("'.$id.'", [', $vue);
				$ziegels[$id] = $vue;
			}

			$data = str_replace('<!--%ziegels%-->', '<script>define("Ziegels", function(){'
				.implode(PHP_EOL, $ziegels)
				.'; return ["'.implode('","', array_keys($ziegels)).'"]});</script>', $data);
		}

		return $data;
	}

	public function process(){
		$config = $this->read_config($this->entry_path);
		$ziegel = $this->load_ziegel($config['ziegel']);
		$base_file = $this->load_ziegel_base($ziegel);
		$this->out($this->place_variables($base_file, $config, $ziegel));
	}

}